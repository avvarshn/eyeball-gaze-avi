# Overview

This project is a fork from the original project **eyeball-gaze-project** developed at University of California, Santa Cruz under professor Roberto Manduchi. 



The goal of this project is to perform head pose estimations with the 3D point cloud data fed in from the **Intel D435** camera versus the original project which performs head pose estimations with a 2D color image. Since 3D point cloud data is always more accurate then 2D data, head pose estimations from this data set would be significantly more accurate.



Once head pose estimations from a 3D point set have been calculated it will then be compared against head pose estimations from the 2D data set. This is to judge how well the 2D head pose algorithm is and correct it in the best way possible.



This fork of the project allows a user to use an **Intel D435** stereo vision camera to perform a variety of facial operations as listed below.

- Face Tracking
- Facial Landmark Detection (68 Point)
- Head Pose Estimation (Roll, Pitch, Yaw)
- Eye Gaze Estimation (Pitch, Yaw)



### Plan of Action

1. Obtain color and depth images from **Intel D435** camera.
2. Run OpenCV’s face detection algorithm (Haar Cascade Classifier) on the incoming camera frames.
3. Remove everything from depth image that is farther then depth of face and slightly closer. This will allows only the face 3D point cloud to be obtain by removing anything too far or too close.
4. Use the *Point Cloud Tracking* or PCL library to run object detection and tracking on this face point cloud.

At this point the code which compares the 3D and 2D head pose estimations needs to be built. Before this can be done, calibration needs to be done against the two cameras. This is because the two cameras are in different spots or coordinates points in the real world, so their incoming images are slightly different, since different perspectives.

5. Camera rotation/translation matrices need to be calculated. One is as such...
   1. Run face detection on both webcam and D435 camera frames. Align the two images such that the center point of each detected face overlaps. Meaning that the *nose* or *eyes* in both images overlap if you were to place one camera image over the other.
      1. Because we are overlapping one image on top of another, we will know in *exact* pixels the amount of image shift required to fit one on top of the other.
   2. We can then crop out each image to only obtain an image slightly larger then the detected face region.
   3. Let PCL perform head pose estimation on the D435 image (color + 3D point cloud) and use the 2D algorithm for head pose estimation on the webcam color image.
   4. Use an equation to map either set of estimations in terms of reference frame of the other! At this point you will have found any differences between head pose estimation outputs.



### Project Tree

* EyeGaze - Root directory of project
  * EyeGazeDemo.sln - This is the MSVC project file. **Double Click or open this file in MSVC to load your project.**
  * EyeGazeDemo - Contains source, binaries, DLLs, and assets
    * Release - Binary if project is built in *release* mode along with all required DLLs
    * Debug - Binary if project is built in *debug* mode.
  * Libraries - Contains *include* and *lib* files for required external libraries.
    * IntraFace - Offers head detection, tracking, and estimations.
    * OpenCV - Open Computer Vision Library (version 2.4.9)



# Installation & Build Guide

This section will go over all the steps required to download, setup, and build this project. Please satisfy all requirements before trying to build.



### Requirements

- Windows 7 (x86 / x64) or Windows 10 (x86 / x64)
- Microsoft Visual Studio 2017



### External Dependencies Downloads

Note that whenever you see **$(ProgramFiles)**, this will be referring to one of two locations as listed below.

- "C:\Program Files (x86)"
- "C:\Program Files"

This is done because based on either x64 or x86 systems, certain applications install to different "Program File" directories. Instead of constantly referring to both locations we will use this substitution.



We will first need to install and setup all required drivers and modules to be able to program for the **Intel D435** camera.

1.  Download the ***Intel RealSense SDK 2.0 (Build 2.13.0)***. This SDK is constantly updating and new builds may be released. All source has currently only been tested with **Build 2.13.0**, but future builds *should* be compatible. The following link will let you download the required *.exe* file for installing the RealSense SDK.
    - https://github.com/IntelRealSense/librealsense/releases/download/v2.13.0/rssetup-2.13.0.18.exe

2. You will need to download run it and follow the installation prompts. This will install the SDK along with an application that will allow you to see the camera feed for both depth and color images.
   - SDK should install to: *$(ProgramFiles)\Intel RealSense SDK 2.0* 
   3. The application used to view D435 camera streams is **realsense-viewer.exe**. You may find this application at the following location: *$(ProgramFiles)\Intel RealSense SDK 2.0\tools*



### Source Download

1. Begin by either cloning the BitBucket project repository or downloading a zip file of the project. If you simply download the project as a *zip* file you will **not** be able to use*git* commands. Downloading a *zip* of a git repo. does not set up the required *.git*/ files. If you wish to be able to make git commits or pushes please clone the repo. via git. Once the repo. has been cloned go ahead and open up your project with Microsoft Visual Studio. (Please see section **Project Tree** to know which file to open with Visual Studio).

   `git clone ttps://avvarshn@bitbucket.org/avvarshn/eyeball-gaze-avi.git`

2. Now to build the project change the build type to **Release** and windows version to **Win32**. These two settings can be made at the very top of the Visual Studio IDE next to the **Local Windows Debugger** button.

   1. All dependencies have been set to use a relative path from the project’s top directory. This means that instead of using an *absolute* path like **C:\Users\Avi\Documents\Project\Libraries\OpenCV\include** the project uses paths such as **..\Libraries\OpenCV\include**.
   2. In case you do have dependency path issues you may try to see where the problem lies and fix it or contact a maintainer.

3. Once the project has been built, it is best to run it through Windows **Command Prompt**. This is because many times the Windows debugger tries to run library debugging on a executing program, which results in crashes for no reason.

   1. To run via command line **cd** into directory where the projects builds and puts its binary. Below is an example path of where to go to: “C:\Users\Avi\Documents\Thesis\EyeGaze\EyeGazeDemo\Release”. *Note that for each user the paths will be different, but once inside your project’s top directory go into the **Release** folder.*
   2. Once here you will find a binary called **EyeGazeDemo.exe**, go ahead and run this from Command Prompt and shortly after you will see the different features such as head tracking and more running on the **RealSense D435** camera. (Of-course don’t forget to plug it in first!)


#ifndef EYEGAZE_H
#define EYEGAZE_H

/* C++ libraries. */
#include <exception>
#include <vector>
#include <set>

/* OpenCV libraries. */
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

/* User defined libraries. */
#include "eyegaze.h"
#include "adaptivecanny.h"
#include "newhough.h"
#include "newhough.h"

//#include <EllipseLib.h>

/* Define taylored exceptions. */
class ImageTypeException: public std::exception {
	virtual const char* what() const throw() {
    return "Eye image type error: eye image must be BGR!";
  }
};

class ImageSizeException: public std::exception {
  virtual const char* what() const throw() {
    return "Eye image is void!";
  }
};

class IrisDetectionException: public std::exception {
  virtual const char* what() const throw() {
    return "Iris contour not found!";
  }
};

class EyeGaze{
	public:
		EyeGaze() {}

	// Define constants
	static const int MIN_IRIS_RADIUS = 5; // mm
	static const int MAX_IRIS_RADIUS = 7; // mm

	/* Black and white image of equalized eye. */
	cv::Mat equalizedEye;
	/* Stores eye edges. */
	cv::Mat eyeEdges;
	/* Depth scaled radiua range. */
	std::vector<int> range;

	NewHough hough;
	
	/* Esimates iris center given an RGB image.
   *
	 * @param RGB image of eye.
   * @param Camera intrinsics matrix.
   * @param Eye depth in mm.
   * @param Enable/disable thresholding.
   * @param Enable/disable maximum density option.
   * @param Bilateral filter sigma color.
   * @param Bilateral filter sigma space.
   * @return Iris center coordinates and radius [pxl].
   */
	cv::Vec3i computeIrisCenter(cv::Mat eye, 
															cv::Mat K,
															float depth,
															bool threshold = false,
															bool maxD = false,
															int sColor = 0,
															int sSpace = 0,
															int options = NewHough::QUANTIZED_GRADIENT);

	NewHough::EllipseHough computeIrisCenterEllipse(cv::Mat eye,
																									cv::Mat K,
																									float depth,
																									bool threshold = false,
																									bool maxD = false,
																									int sColor = 0,
																									int sSpace = 0);

    /*Estimates head position in 3D space.
		 *
     * @param head rotation matrix
     * @param camera intrinsics matrix
     * @param left eye anchor point on image plane [pxl]
     * @param right eye anchor point on image plane [pxl]
     * @param eyes height from nose anchor point [mm]
     * @param eyes distance [mm]
     * @return head 3D position coordinates [mm]
     */
    cv::Point3i computeHeadPosition(cv::Mat R, cv::Mat K, cv::Point El, cv::Point Er, int H, int D );

	/**
     * @brief Method for computing the backprojection of an image plane point to a 3D plane
     * @param head rotation matrix
	 * @param head 3D position coordinates [mm]
     * @param camera intrinsics matrix
     * @param point on image plane to backproject [pxl]
     * @return backprojected point 2D cooridnates on 3D plane [mm]
     */
    cv::Point2i projectOnPlane(cv::Mat R, cv::Point3i position, cv::Mat K, cv::Point point2D);

	/* Returns black and white equalized image. */
	inline cv::Mat getEqualizedEye(){
		return this->equalizedEye;
	}
	 
	/* Returns image of eye edges. */
	inline cv::Mat getEyeEdges(){
		return this->eyeEdges;
	}

	/* Method for returning the eye iris radius range scaled by head depth. */
	inline std::vector<int> getIrisRange(){
		return range;
	}

	/* Method for returning the hough circles accumulator. */
	inline std::vector<std::vector<std::vector<double>>> getHoughAccumulator(){
		return hough.getHoughAccumulator();
	}
};

#endif // EYEGAZE_H

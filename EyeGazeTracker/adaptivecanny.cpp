// Include required standard headers
#include <vector>
#include <cmath>

// Include required headers
#include "adaptivecanny.h"

using namespace std;
using namespace cv;

// adaptiveCanny
Mat AdaptiveCanny::adaptiveCanny(Mat img, Mat& GX, Mat& GY, Mat& mag,  int& maxTH, int& minTH){
    /*
    * Compute adaptive thresholds
    */
    // Magic numbers
    const double PercentOfPixelsNotEdges = 0.9; // Used for selecting thresholds
    const double ThresholdRatio = 0.3;        // Low thresh is this fraction of the high.
	//const double ThresholdRatio = 0.1;        // Low thresh is this fraction of the high.
    const double sigma = sqrt(2);

    // Convert to double matrix
    Mat imgd (img.rows, img.cols, CV_64F);
    img.convertTo(imgd, CV_64F,1.0/255,0);

    // Calculate gradients using a derivative of Gaussian filter
    //Mat GX (0,0,CV_64F);
    //Mat GY (0,0,CV_64F);
    //smoothGradient(imgd, sigma, GX, GY);
    Mat grad_x, grad_y;
    Mat abs_grad_x, abs_grad_y;
    int scale = 1;
      int delta = 0;
     int ddepth = CV_32F;
    Sobel( img, grad_x, ddepth, 1, 0, 3, scale, delta, BORDER_DEFAULT );
    // convertScaleAbs( grad_x, abs_grad_x );
    Sobel( img, grad_y, ddepth, 0, 1, 3, scale, delta, BORDER_DEFAULT );
     // convertScaleAbs( grad_y, abs_grad_y );
      Mat magGrad, pgx, pgy; 
	  pow(grad_x,2,  pgx);
	  pow(grad_y,2,  pgy);
	  magGrad = pgx+pgy;
	  
	  cv::sqrt(magGrad, magGrad);
      //addWeighted( abs_grad_x, 0.5, abs_grad_y, 0.5, 0, magGrad );

    // Calculate Magnitude of Gradient
   // Mat magGrad (img.rows, img.cols, CV_64F);

      GX = grad_x;
      GY = grad_y;
	  mag = magGrad;
    //cv::magnitude(GX, GY, magGrad);
    // Normalize for threshold selection
    double minVal;
    double maxVal;
    Point minLoc;
    Point maxLoc;
    //minMaxLoc( magGrad, &minVal, &maxVal, &minLoc, &maxLoc );
    //magGrad /= maxVal;

    // Determine Hysteresis Thresholds
    double lowThresh, highThresh;
    selectThresholds(magGrad, PercentOfPixelsNotEdges, ThresholdRatio, lowThresh, highThresh);

    // Invoke normal Canny

    Mat edges (img.rows, img.cols, CV_8U);
    // Copy matrix to not ruin original one
    Mat imgCopy = img.clone();
    GaussianBlur(imgCopy, imgCopy, Size(15,15), 2,2); // TODO: reduce filter size
    //cv::Canny(imgCopy, edges, lowThresh*255, highThresh*255, 3, true);
	cv::Canny(imgCopy, edges, 60, 128, 3, true);

    // Perform Non-Maximum Suppression Thining and Hysteresis Thresholding of Edge strength
    //edges = thinAndThreshold(GX, GY, magGrad, lowThresh, highThresh);
	maxTH = 255*highThresh;
	minTH = 255*lowThresh;

    return edges;
}


// Define utility methods
void AdaptiveCanny::smoothGradient(Mat img, double sigma, Mat &GX, Mat &GY){
    // Create an even-length 1-D separable Derivative of Gaussian filter

    // Determine filter length
    int filterLength = 8*ceil(sigma);
    // Get gaussian kernel normalized
    Mat gKernel = getGaussianKernel(filterLength, sigma, CV_64F);
    Mat dgKernel (filterLength,1, CV_64F);
    // Get derivative kernel
    Mat kernelx (1, 3, CV_64F);
    kernelx.at<double>(0,0) = -0.5;kernelx.at<double>(0,1) = 0;kernelx.at<double>(0,2) = 0.5;
    cv::filter2D(gKernel, dgKernel, -1, kernelx.t());

    // Normalize to ensure kernel sums to zero
    vector<int> negVals;
    vector<int> posVals;
    for (int i = 0; i < dgKernel.rows * dgKernel.cols; ++i)
        if (dgKernel.at<double>(i, 0) > 0)
            posVals.push_back(i);
        else if (dgKernel.at<double>(i, 0) < 0)
            negVals.push_back(i);

    double s = 0;
    for (int i = 0; i < posVals.size(); ++i)
        s += dgKernel.at<double>(posVals[i], 0);
    for (int i = 0; i < posVals.size(); ++i)
        dgKernel.at<double>(posVals[i], 0) /= s;
    s = 0;
    for (int i = 0; i < negVals.size(); ++i)
        s += dgKernel.at<double>(negVals[i], 0);
    s = abs(s);
    for (int i = 0; i < negVals.size(); ++i)
        dgKernel.at<double>(negVals[i], 0) /= s;

    // Compute smoothed numerical gradient of image I along x (horizontal)
    // direction. GX corresponds to dG/dx, where G is the Gaussian Smoothed
    // version of image I.

    Mat tmpx = GX.clone();
    Mat tmpy = GY.clone();
    flip (gKernel, gKernel, -1);
    flip (dgKernel, dgKernel, -1);
    filter2D(img, tmpx, CV_64F, gKernel);
    filter2D(tmpx, GX, CV_64F, dgKernel.t());

    // Compute smoothed numerical gradient of image I along y (vertical)
    // direction. GY corresponds to dG/dy, where G is the Gaussian Smoothed
    // version of image I.

    filter2D(img, tmpy, CV_64F, gKernel.t());
    filter2D(tmpy, GY, CV_64F, dgKernel);

}

// selectThresholds
void AdaptiveCanny::selectThresholds(Mat magGrad, double PercentOfPixelsNotEdges, double ThresholdRatio, double& lowThresh, double& highThresh){
    int m = magGrad.rows;
    int n = magGrad.cols;

    // Select the thresholds
    vector<int> channels;
    channels.push_back(0);
    MatND counts;
    vector<int> size;
    size.push_back(64);
    vector<float> range;
    range.push_back(0);
    range.push_back(255);
    vector<Mat> vec;
    //Mat tmp (magGrad.rows, magGrad.cols, CV_32F);
    //magGrad.convertTo(tmp, CV_32F);
    vec.push_back(magGrad);
    calcHist(vec, channels, Mat(), counts, size, range, true);

    // Compute highThreshold
    vector<int> csum (counts.rows);
    csum[0] = counts.at<float>(0);
    for (int i = 1; i < counts.rows; ++i)
        csum[i] = counts.at<float>(i) + csum[i-1];

    for (int i = 1; i < counts.rows; ++i)
        if (csum[i] > PercentOfPixelsNotEdges*m*n){
            highThresh = i / 64.0;
            break;
        }

        lowThresh = ThresholdRatio*highThresh;
}

// DEPRECATED
/*
vector<int> cannyFindLocalMaxima(int direction, Mat _ix,Mat _iy, Mat _mag) {

    //% This sub-function helps with the non-maximum suppression in the Canny
    //% edge detector.  The input parameters are:
    //%
    //%   direction - the index of which direction the gradient is pointing,
    //%               read from the diagram below. direction is 1, 2, 3, or 4.
    //%   ix        - input image filtered by derivative of gaussian along x
    //%   iy        - input image filtered by derivative of gaussian along y
    //%   mag       - the gradient magnitude image
    //%
    //%    there are 4 cases:
    //%
    //%                         The X marks the pixel in question, and each
    //%         3     2         of the quadrants for the gradient vector
    //%       O----0----0       fall into two cases, divided by the 45
    //%     4 |         | 1     degree line.  In one case the gradient
    //%       |         |       vector is more horizontal, and in the other
    //%       O    X    O       it is more vertical.  There are eight
    //%       |         |       divisions, but for the non-maximum suppression
    //%    (1)|         |(4)    we are only worried about 4 of them since we
    //%       O----O----O       use symmetric points about the center pixel.
    //%        (2)   (3)

	Mat ix = _ix.clone();
	Mat iy = _iy.clone();
	Mat mag = _mag.clone();
	int m = mag.rows;
	int n = mag.cols;

	// Find the indices of all points whose gradient (specified by the
	// vector (ix,iy)) is going in the direction we're looking at.
	vector<int> idx;
	switch (direction){
	case 1:{
		unsigned char *rowPtrX;
		unsigned char *rowPtrY;
		for (int i = 0; i < m; ++i){
			rowPtrX = ix.ptr<unsigned char>(i);
			rowPtrY = iy.ptr<unsigned char>(i);
			for (int j = 0; j < n; ++j)
				if((rowPtrY[j] <=0 && rowPtrX[j] >-rowPtrY[j])| (rowPtrY[j] >=0 && rowPtrX[j] <-rowPtrY[j]))
					idx.push_back(n * i + j);
		}             
		   } break;
	case 2:{
		unsigned char *rowPtrX;
		unsigned char *rowPtrY;
		for (int i = 0; i < m; ++i){
			rowPtrX = ix.ptr<unsigned char>(i);
			rowPtrY = iy.ptr<unsigned char>(i);
			for (int j = 0; j < n; ++j)
				if((rowPtrX[j] > 0 && -rowPtrY[j] >= rowPtrX[j])  || (rowPtrX[j] < 0 && -rowPtrY[j] <= rowPtrX[j]))
					idx.push_back(n * i + j);           
		}
		   } break;
	case 3: {
		unsigned char *rowPtrX;
		unsigned char *rowPtrY;
		for (int i = 0; i < m; ++i){
			rowPtrX = ix.ptr<unsigned char>(i);
			rowPtrY = iy.ptr<unsigned char>(i);
			for (int j = 0; j < n; ++j)
				if((rowPtrX[j] <= 0 && rowPtrX[j] > rowPtrY[j]) || (rowPtrX[j] >=0 && rowPtrX[j] < rowPtrY[j]))
					idx.push_back(n * i + j); 
		}
			} break;
	case 4:{
		unsigned char *rowPtrX;
		unsigned char *rowPtrY;
		for (int i = 0; i < m; ++i){
			rowPtrX = ix.ptr<unsigned char>(i);
			rowPtrY = iy.ptr<unsigned char>(i);
			for (int j = 0; j < n; ++j)
				if((rowPtrY[j] < 0 && rowPtrX[j] <= rowPtrY[j]) || (rowPtrY[j] > 0 && rowPtrX[j] >= rowPtrY[j]))
					idx.push_back(n * i + j); 
		}
		   } break;
	}

	// Exclude the exterior pixels
	vector<int> tmpVec;
	for (int i = 0; i < idx.size(); ++i){
		int u = idx[i] % n;
		int v = idx[i] / n;
		if (u > 0 && u < (n-1) && v > 0 && v < (m-1))  
			tmpVec.push_back(idx[i]); 
	}
	idx = tmpVec;

	// Get only derivatives with retained indexes
	vector<double> ixv(idx.size());
	vector<double> iyv(idx.size());
	vector<double> gradmag(idx.size());
	for (int i = 0; i < idx.size(); ++i){
		int u = idx[i] % n;
		int v = idx[i] / n;
		ixv[i] = ix.at<double>(v, u);
		iyv[i] = iy.at<double>(v, u);
		gradmag[i] = mag.at<double>(v, u);
	}
	
	// Do the linear interpolations for the interior pixels
	vector<double> gradmag1 (idx.size());
	vector<double> gradmag2 (idx.size());
	vector<double> d (idx.size());	

	switch (direction){
	case 1:{
		// Compute d
		for (int i = 0; i < idx.size(); ++i)			
			d[i] = abs(iyv[i] / ixv[i]);
		// Compute magnitudes
		for (int k = 0; k < idx.size(); ++k){
			int j = idx[k] % n;
			int i = idx[k] / n;
			gradmag1[k] = mag.at<double>((m*j+i + m)%m, (m*j+i + m - (m*j+i + m)%m)/m) *(1-d[k]) + mag.at<double>((m*j+i +m-1)%m, (m*j+i + m - 1 - (m*j+i +m-1)%m)/m) *d[k];
			gradmag2[k] = mag.at<double>((m*j+i -m)%m, (m*j+i - m - (m*j+i -m)%m)/m) *(1-d[k]) + mag.at<double>((m*j+i -m+1)%m, (m*j+i - m + 1 - (m*j+i -m+1)%m)/m) *d[k];
		}
	} break;

	case 2:{
		// Compute d
		for (int i = 0; i < idx.size(); ++i)			
			d[i] = abs(ixv[i] / iyv[i]);

		// Compute magnitudes
		for (int k = 0; k < idx.size(); ++k){
			int j = idx[k] % n;
			int i = idx[k] / n;
			gradmag1[k] = mag.at<double>((m*j+i - 1)%m, (m*j+i - 1 - (m*j+i - 1)%m)/m) *(1-d[k]) + mag.at<double>((m*j+i + m - 1)%m, (m*j+i + m - 1 - (m*j+i + m - 1)%m)/m) *d[k];
			gradmag2[k] = mag.at<double>((m*j+i + 1)%m, (m*j+i + 1 - (m*j+i + 1)%m)/m) *(1-d[k]) + mag.at<double>((m*j+i - m + 1)%m, (m*j+i - m + 1 - (m*j+i - m + 1)%m)/m) *d[k];
		}
	} break;

	case 3:{
		// Compute d
		for (int i = 0; i < idx.size(); ++i)			
			d[i] = abs(ixv[i] / iyv[i]);
		
		// Compute magnitudes
		for (int k = 0; k < idx.size(); ++k){
			int j = idx[k] % n;
			int i = idx[k] / n;
			gradmag1[k] = mag.at<double>((m*j+i - 1)%m, (m*j+i - 1 - (m*j+i - 1)%m)/m) *(1-d[k]) + mag.at<double>((m*j+i - m - 1)%m, (m*j+i - m - 1-(m*j+i - m - 1)%m)/m) *d[k];
			gradmag2[k] = mag.at<double>((m*j+i + 1)%m, (m*j+i + 1-(m*j+i + 1)%m)/m) *(1-d[k]) + mag.at<double>((m*j+i + m - 1)%m, (m*j+i + m + 1 - (m*j+i + m + 1)%m)/m) *d[k];
		}
	} break;

	case 4:{
		// Compute d
		for (int i = 0; i < idx.size(); ++i)			
			d[i] = abs(iyv[i] / ixv[i]);

		// Compute magnitudes
		for (int k = 0; k < idx.size(); ++k){
			int j = idx[k] % n;
			int i = idx[k] / n;
			gradmag1[k] = mag.at<double>((m*j+i - m)%m, (m*j+i - m - (m*j+i - m)%m)/m) *(1-d[k]) + mag.at<double>((m*j+i - m - 1)%m, (m*j+i - m - 1 - (m*j+i - m - 1)%m)/m) *d[k];
			gradmag2[k] = mag.at<double>((m*j+i + m)%m, (m*j+i + m-(m*j+i + m)%m)/m) *(1-d[k]) + mag.at<double>((m*j+i +m+1)%m, (m*j+i + m + 1-(m*j+i +m+1)%m)/m) *d[k];
		}
	}
}



	vector <int> idxLocalMax;
	for (int i = 0; i < idx.size(); ++i)
		if (gradmag[i] >=gradmag1[i] && gradmag[i] >=gradmag2[i])
				idxLocalMax.push_back(idx[i]);
				
	return idxLocalMax;
}

Mat thinAndThreshold(Mat dx, Mat dy, Mat magGrad, double lowThresh, double highThresh){
	int m = magGrad.rows;
	int n = magGrad.cols;
	// Create empty edge map
	Mat H = Mat::zeros(magGrad.rows, magGrad.cols, CV_8U);

	// Perform Non-Maximum Suppression Thining and Hysteresis Thresholding of Edge Strength

	// We will accrue indices which specify ON pixels in strong edgemap
	// The array e will become the weak edge map.
	vector<int> idxStrong;
	for (int dir = 1; dir < 5; ++dir){
		vector<int> idxLocalMax = cannyFindLocalMaxima(dir,dx,dy,magGrad);
		vector<int> idxWeak;
		for (int j = 0; j < idxLocalMax.size(); ++j)
			if (magGrad.at<double>(idxLocalMax[j]/n, idxLocalMax[j]%n) > lowThresh)
				idxWeak.push_back(idxLocalMax[j]);

		for (int j = 0; j < idxWeak.size(); ++j)
			H.at<unsigned char>(idxWeak[j]/n, idxWeak[j]%n)=255;

		for (int j = 0; j < idxWeak.size(); ++j)
			if (magGrad.at<double>(idxWeak[j]/n, idxWeak[j]%n) > highThresh)
				idxStrong.push_back(idxWeak[j]);
		
	}


	return H;
}
*/

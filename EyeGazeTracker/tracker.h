#ifndef TRACKER_H
#define TRACKER_H

#include <librealsense2/rs.hpp> // Include RealSense Cross Platform API

/* Windows libraries. */
#include <windows.h>
#include <Wingdi.h>
#include <Windows.h>
#include <WinUser.h>

/* C++ libraries. */
#include <math.h>
#include <fstream>

/* OpenCV libraries. */
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/video/video.hpp>
#include <opencv2/imgproc/imgproc.hpp>

/* Intrface libraries. */
#include <intraface/FaceAlignment.h>
#include <intraface/XXDescriptor.h>

/* User defined libraries. */
#include "eyegaze.h"
#include "trainer.h"
#include "newhough.h"

/* TODO: */
#define isnan(x)																_isnan(x)
#define isinf(x)																(!_finite(x))
#define fpu_error(x)														(isinf(x) || isnan(x))

/* Files used for face tracking/detection. These files contain data that algorithms require. */
#define FILE_DETECTION_MODEL										"models/DetectionModel-v1.5.bin"
#define FILE_TRACKING_MODEL											"models/TrackingModel-v1.10.bin"
#define FILE_FACE_DETECTION_MODEL								"models/haarcascade_frontalface_alt2.xml"

#define VALID_FACE_DETECTION_SCORE							0.3

/* Used by Tracker::drawHeadPoseAxis(...) to draw axis coordinate system. */
/* Each axis line's thickness. */
#define HEAD_POSE_AXIS_LINE_THICKNESS						2
/* Each axis line's line type. */
#define HEAD_POSE_AXIS_LINE_TYPE								8
/* Offset to apply to axis matrix, otherwise lines would be draw off screen. */
#define HEAD_POS_AXIS_OFFSET										70
/* How fast program should record video stream. */
#define FRAMES_PER_SECOND												5
/* Number of frames to record. */
#define FRAMES_TO_RECORD												50
/* Different keycodes for keys user may press to interact with program. */
#define WAITKEY_DELAY														5
#define KEYCODE_ESCAPE													27
#define KEYCODE_COMMA														44
#define KEYCODE_MINUS														45
#define KEYCODE_PERIOD													46
#define KEYCODE_NUM_0														48
#define KEYCODE_NUM_9														57
#define KEYCODE_PLUS														61
#define KEYCODE_OPEN_BRACKET										91
#define KEYCODE_LOW_APLH_M											109
#define KEYCODE_LOW_APLH_O											111
#define KEYCODE_LOW_APLH_P											112
#define KEYCODE_LOW_APLH_X											120
#define KEYCODE_LOW_APLH_Z											122
/* TODO: */
#define PHYSICAL_SCREEN_WIDTH_MM								310.0
/* TODO: */
#define MIN_GP_RADIUS														50
/* TODO: */
#define MAX_GATE																10.0
/* TODO: */
#define COEF_IIR_ANCHOR													0.5
/* TODO: */
#define COEF_IIR_IRIS														0.5
/* Inter-eye distance in mm. Note: this is already somewherer in Qt.
 * Must calibrate it so that the distances measured make sense.
 */
#define D_EC12																	-90		// -95
 /* These are set by the Matlab calibration code (MCalibrateV*)
  * this is only used if two feature points are used . Now usinng a single point.
  */
/* //0.76976; //0.597; //0.6730; // 0.4515 */
#define EYE_COEF_1															0.686
/* // 53.; //9.1378; //2.4798; //4.6811; //0.0416; //-14; //-12; */
#define P_R1_X																	14.0
/* // -0.96548; //0.96548-0.4130; //-0.2556; //-0.7019; */
#define P_R1_Y																	25.0
/*  //3.6285; //3.1967; //3.4115; //2.5667;// 0.; //-11.; */
#define P_R1_Z																	18.0

#define MAX_EYE_DIST														20.0

/* Compares two OpenCV rectangles.
 *
 * @Param[0]	First rectangle.
 * @Param[1]	Second rectangle.
 * @Return	True if first rect. height is less then second rect. height.
 *					False otherwise.
 */
bool CompareRect(cv::Rect, cv::Rect);

class Tracker {
public :
	Tracker(void);

	/* Main function which does facial/feature tracking.
	 *
	 * @Param[0] Camera video stream to use for tracking.
	 * @Param[1] If true it records certain values and debugging information.
	 */
	void run(cv::VideoCapture, bool);

	void run(bool);

	bool init(void);

private:
	bool FLAG_PAUSE;
	bool detectFace;
	float score;
	cv::Mat X, X0;
	int keyCode;

	int CALIBRATE;
	int TAKE_SNAPSHOT;
	int IS_FIRST;
	int KALMAN;
	int IIR_FILTER;
	int NFRAMESTORECORD;
	int RECORD;
	int EYECENTER1;
	float eyeRadius_MM;

	bool PAUSE_FLAG;

	float margVarX, margCovXX;
	float coefIIR_anchor, coefIIR_iris, coefIIR_Z;
	float lastZ;
	bool maxDensity, binaryThresholding;

	NewHough::OPTIONS irisCenterDetectionMode;

	Trainer::MODEL gazeModel;

	INTRAFACE::FaceAlignment *faceAlignment;

	cv::CascadeClassifier *faceCascadeClassifier;

	cv::KalmanFilter kalmanFilter;
	cv::Point oldEl_i;
	cv::Point lastEl_i, lastrCircleDisp;
	cv::Point3i gpCircle0, gpCircle1, gpCircle2;

	cv::Mat K;

	std::ofstream calibrationFile, recordFile;

	bool isInitialized;

	EyeGaze leftGaze, rightGaze;

	string videoName;
	string outputName;

	cv::VideoWriter video;
	cv::Mat lEye;
	cv::Mat rEye;
	cv::Mat lEyeBW;
	cv::Mat rEyeBW;
	cv::Mat leftEyeBW;
	cv::Mat rightEyeBW;
	cv::Vec3i lCircle;
	cv::Vec3i rCircle;
	cv::Point	rCircleDisp;

	const cv::Scalar CV_COLOR_RED = cv::Scalar(255, 0, 0);
	const cv::Scalar CV_COLOR_GREEN = cv::Scalar(0, 255, 0);
	const cv::Scalar CV_COLOR_BLUE = cv::Scalar(0, 0, 255);

	/* Displacement vector used to compute the eyeball center. */
	float P_R1[3] = { P_R1_X, P_R1_Y, P_R1_Z };
	cv::Mat P_R1_E = cv::Mat(3, 1, CV_32F, (float *) P_R1);

	/* Gives resolution of current screen window. Refers to actuall screen resolution of device.
	 *
	 * @Param[0] Saves screen resolution width into this variable.
	 * @Param[1] Saves screen resolution height into this variable.
	 */
	void getDesktopResolution(int &, int &);

	/* Draws head position in 3D coordinate space at top right of window. */
	void drawHeadPoseAxis(cv::Mat &, const cv::Mat &, float);

	bool detectFaces(cv::Mat frame, std::vector<cv::Rect> faces);

	void initKalmanFilter(void);

	void handleKeyPress(int);
};

#endif // TRACKER_H

/* C++ libraries. */
#include <math.h>
#include <fstream>
#include <iostream>

/* User defined libraries. */
#include "newhough.h"

#define PI 3.14159265

//Point2d directions[12] = { Point2d(1,0), Point2d(1.73,0.5), Point2d(0.5,1.73),
//													 Point2d(0,1), Point2d(-0.5,1.73), Point2d(-1.73,0.5),
//													 Point2d(-1,0), Point2d(-1.73, -0.5), Point2d(-0.5,-1.73),
//													 Point2d(0,-1), Point2d(0.5,-1.73), Point2d(1.73,-0.5) };
//
//Point2f directions[8] = { Point2f(0, 1,0), Point3f(45, 0.71, 0.71), Point3f(90, 0,1),
//													Point3f(135, -0.71, 0.71), Point3f(180, -1,0), Point3f(225, -0.71, -0.71),
//													Point3f(270, 0,-1), Point3f(325, 0.71, -0.71) };

bool ComparePoints(Point4i i, Point4i j) {
	return (j.val < i.val);
}
bool ComparePoints2(TPoint6m i, TPoint6m j) {
 return (i.u < j.u);
}
std::vector<std::vector<TPoint5i>> lutEllipse;

NewHough::NewHough() {
	// Init general data structures for efficency
	templates = std::vector<std::vector<cv::Point2i>>(100, std::vector<cv::Point2i>());
	std::vector<int> radiuses(100);
	for (int i = 0; i < 100; ++i)
		radiuses[i] = i + 1;
	for (int i = 0; i < 100; ++i)
		templates[i] = circlepoints(radiuses[i]);
	lut = std::vector<std::vector<cv::Point3i> > (getLUT(radiuses));

	lutEllipse = getEllipseLUT(radiuses);
}

std::vector<cv::Vec3i> NewHough::circle_hough(cv::Mat edges,
																							std::vector<int> range,
																							bool same,
																							bool normalise,
																							int npeaks,
																							cv::Mat GX,
																							cv::Mat GY,
																							cv::Mat mag,
																							int options)
{
	// Get indices of the edge coordinates
	std::vector<cv::Point2i> idx;
	unsigned char *rowPtr;

	for (int i = 0; i < edges.rows; ++i) {
		rowPtr = edges.ptr<unsigned char>(i);
		
		for (int j = 0; j < edges.cols; ++j) {
			if (rowPtr[j] > 0)
				idx.push_back(cv::Point2i(i, j));
		}
	}

	/* Return empty vector if no circles found. */
	if (idx.size() < 1)
		return std::vector<cv::Vec3i>();

	/* Quantize gradients along edges. */
	std::vector<int> qGrad(idx.size());
	for (int i = 0; i < idx.size(); ++i) {
		int dir;

		if (GY.at<float>(idx[i].x, idx[i].y) < 0)
			dir = (360 + atan2(GY.at<float>(idx[i].x, idx[i].y), GX.at<float>(idx[i].x, idx[i].y)) * 180 / PI) / 45.0;
		else
			dir = (atan2(GY.at<float>(idx[i].x, idx[i].y), GX.at<float>(idx[i].x, idx[i].y)) * 180 / PI) / 45.0;
		qGrad[i] = dir;
	}

	/* Set up accumulator array with margin to avoid buffer overflows. No longer need to check bounds. */
	int nr = edges.rows;
	int nc = edges.cols;
	int nradii = range.size();
	int margin = ceil(*(std::max_element(range.begin(), range.end()))) + 2;

	int nrh = nr + 2 * margin + 1;
	int nch = nc + 2 * margin + 1;
	hAccumulator = std::vector< std::vector< std::vector<double>>>
										(nrh, std::vector< std::vector<double>>(nch, std::vector<double>(nradii, 0)));

	double maxMag, minMag;
	minMaxLoc(mag, &minMag, &maxMag);

	/* Compute Hough Transform. */
	if (options & OPTIONS::NO_GRADIENT) {
		/* Do not use edge gradient directions. */
		for (int i = 0; i < nradii; ++i) {
			for (int f = 0; f < idx.size(); ++f) {
				for (int j = 0; j < templates[range[i] - 1].size(); ++j) {
					cv::Point2i pt = templates[range[i]][j];
					
					if (options & OPTIONS::EDGE_WEIGHTING) {
						if ((pt.x + idx[f].x) > 0 && (pt.y + idx[f].y) > 0 && (pt.x + idx[f].x) < edges.rows && (pt.y + idx[f].y) < edges.cols)
							hAccumulator[pt.x + idx[f].x + margin][pt.y + idx[f].y + margin][i] += mag.at<float>(pt.x + idx[f].x, pt.y + idx[f].y) * 255 / maxMag;
					} else {
						try {
							hAccumulator[pt.x + idx[f].x + margin][pt.y + idx[f].y + margin][i] += 1;
						}
						catch (std::exception e) {
							std::cout << "hcAccumulator exception!\n";
						}
					}
				}
			}
		}
	}

	if (options & OPTIONS::GRADIENT) {
		/* Use edge gradient directions. */
		double gx, gy, rx, ry, prod;

		for (int i = 0; i < nradii; ++i) {
			for (int f = 0; f < idx.size(); ++f) {
				gx = GX.at<float>(idx[f].x, idx[f].y);
				gy = GY.at<float>(idx[f].x, idx[f].y);

				for (int j = 0; j < templates[range[i]].size(); ++j) {
					cv::Point2i pt = templates[range[i]][j];
					/* Get gradient on the current circle point. */
					rx = pt.x;
					ry = pt.y;

					prod = rx * gy + ry * gx;
					if (prod < 0) {
						if (options & OPTIONS::EDGE_WEIGHTING) {
							if ((pt.x + idx[f].x) > 0 && (pt.y + idx[f].y) > 0 && (pt.x + idx[f].x) < edges.rows && (pt.y + idx[f].y) < edges.cols)
								hAccumulator[pt.x + idx[f].x + margin][pt.y + idx[f].y + margin][i] += mag.at<float>(pt.x + idx[f].x, pt.y + idx[f].y) * 255 / maxMag;
						}
						else hAccumulator[pt.x + idx[f].x + margin][pt.y + idx[f].y + margin][i] += 1;
					}
				}
			}
		}
	}

	if (options & OPTIONS::QUANTIZED_GRADIENT) {	
		// Loop over features
		for (int f = 0; f < idx.size(); ++f)
			for (int r = 0; r < range.size(); ++r) {
				// Get all compatible points for the gradient direction
				/*for (int i = 0; i < lut.size(); ++i) {
					for (int j = 0; j < lut[i].size(); j++) {
						std::cout << "lust[" << i << "][" << j << "]: " << lut[i][j] << "\n";
					}
				}*/

				std::vector<cv::Point3i> points = std::vector<cv::Point3i> (lut[8 * (range[r] - 1) + qGrad[f]]);

				for (int i = 0; i < points.size(); ++i) {
					if (options & OPTIONS::EDGE_WEIGHTING)
						if ((points[i].x + idx[f].x) > 0 && (points[i].y + idx[f].y) > 0 && (points[i].x + idx[f].x) < edges.rows && (points[i].y + idx[f].y) < edges.cols)
							hAccumulator[points[i].x + idx[f].x + margin][points[i].y + idx[f].y + margin][r] += mag.at<float>(points[i].x + idx[f].x, points[i].y + idx[f].y) * 255 / maxMag;
						else;
					else try {
						hAccumulator[points[i].x + idx[f].x + margin][points[i].y + idx[f].y + margin][r] += 1;
					}
					catch (std::exception e) {
						std::cout << "hcAccumulator exception!\n";
					}
				}
			}
	}

	/*
	ofstream t ("mag.dat");
	for (int i = 0; i < mag.rows; ++i){
		for (int j = 0; j < mag.cols; ++j)
			t << mag.at<float>(i,j) << " ";
		t << endl;
	}
	t.close();

	t = ofstream ("edges.dat");
	for (int i = 0; i < edges.rows; ++i){
		for (int j = 0; j < edges.cols; ++j)
			t << (int) edges.at<unsigned char>(i,j) << " ";
		t << endl;
	}
	t.close();

	t = ofstream ("gx.dat");
	for (int i = 0; i < GX.rows; ++i){
		for (int j = 0; j < GX.cols; ++j)
			t << GX.at<float>(i,j) << " ";
		t << endl;
	}
	t.close();

	t = ofstream ("gy.dat");
	for (int i = 0; i < GY.rows; ++i){
		for (int j = 0; j < GY.cols; ++j)
			t << GY.at<float>(i,j) << " ";
		t << endl;
	}
	t.close();

	t = ofstream ("dxq.dat");
	for (int i = 0; i < DXq.rows; ++i){
		for (int j = 0; j < DXq.cols; ++j)
			t << DXq.at<float>(i,j) << " ";
		t << endl;
	}
	t.close();

	t = ofstream ("dyq.dat");
	for (int i = 0; i < DYq.rows; ++i){
		for (int j = 0; j < DYq.cols; ++j)
			t << DYq.at<float>(i,j) << " ";
		t << endl;
	}
	t.close();
	*/


	if (same) {
		// Remove peaks in the margin area
		for (int r = 0; r < nradii; ++r)
			for (int i = 0; i < margin + 1; ++i)
				for (int j = 0; j < hAccumulator[0].size(); ++j)
					hAccumulator[i][j][r] = 0;

		for (int r = 0; r < nradii; ++r)
			for (int i = hAccumulator.size() - margin + 1; i < hAccumulator.size(); ++i)
				for (int j = 0; j < hAccumulator[0].size(); ++j)
					hAccumulator[i][j][r] = 0;

		for (int r = 0; r < nradii; ++r)
			for (int i = 0; i < hAccumulator.size(); ++i)
				for (int j = 0; j < margin + 1; ++j)
					hAccumulator[i][j][r] = 0;

		for (int r = 0; r < nradii; ++r)
			for (int i = 0; i < hAccumulator.size(); ++i)
				for (int j = hAccumulator[0].size() - margin; j < hAccumulator[0].size(); ++j)
					hAccumulator[i][j][r] = 0;
	}

	if (normalise)
		for (int r = 0; r < nradii; ++r)
			for (int i = 0; i < hAccumulator.size(); ++i)
				for (int j = 0; j < hAccumulator[0].size(); ++j)
					hAccumulator[i][j][r] /= range[r];

	return circle_houghpeaks(hAccumulator, range, npeaks, margin);
}

// circle_houghpeaks
std::vector<cv::Vec3i>
NewHough::circle_houghpeaks(std::vector< std::vector< std::vector<double>>>& H,
														std::vector<int> range,
														int npeaks, int margin)
{
	std::vector<cv::Vec3i> peaks;

	// Compute max value
	double mv = 0;
	for (int i = 0; i < H.size(); ++i)
		for (int j = 0; j < H[0].size(); ++j)
			for (int k = 0; k < H[0][0].size(); ++k)
				if (H[i][j][k] > mv)
					mv = H[i][j][k];

	// Retain only the 50% of the max value
	//mv *= 0.5;

// Get all the accepted maxima
	std::vector<Point4i> maxima;
	for (int k = 0; k < range.size(); ++k)
		for (int j = 0; j < H[0].size(); ++j)
			for (int i = 0; i < H.size(); ++i)
				if (H[i][j][k] > 0 && H[i][j][k] >= mv) // Val > 0 to avoid NULL maxima!
					maxima.push_back(Point4i(i, j, k, H[i][j][k]));

	// Sort maxima indexes
	std::sort(maxima.begin(), maxima.end(), ComparePoints);

	/*
	// Find regional max

	vector<boolean_T> conn( std::powf(3, range.size()), 1);
	vector<boolean_T> BW (H.size()* H[0].size()* range.size(), 0);
	vector<real64_T> F (H.size()* H[0].size()* range.size());
	real64_T imSize[3] = {H.size(), H[0].size(),range.size()};
	vector<real64_T> connSize (std::powf(3,range.size()), 3);
	vector<vector<vector<int>>> m (3, vector<vector<int>>(3, vector<int>(3)));
	for (int k = 0; k < 3; ++k)
		for (int i = 0; i < 3; ++i)
			for (int j = 0; j < 3; ++j)
				m[i][j][k] = k*3* 3 + 3*i + j;
	vector<int> values;
	for (int k = 0; k < 3; ++k)
		for (int i = 0; i < 3; ++i)
			for (int j = 0; j < 3; ++j)
				values.push_back( m[i][j][k]);

	vector<int> idxm (3*3*3);
	for (int k = 0; k < 3; ++k)
		for (int j = 0; j < 3; ++j)
			for (int i = 0; i < 3; ++i)
				idxm[k*3*3 + 3*j + i] = m[i][j][k];

	vector<int> idx;
	for (int k = 0; k < 3; ++k)
		for (int i = 0; i < 3; ++i)
			for (int j = 0; j < 3; ++j)
				idx.push_back(k*3* 3 + 3*j + i);


	for (int k = 0; k < range.size(); ++k)
		for (int j = 0; j < H[0].size(); ++j)
			for (int i = 0; i < H.size(); ++i)
				F[k*H.size()* H[0].size() + H.size()*j + i] = H[i][j][k];

	imregionalmax_real64(F.data(),
		BW.data(),
				3,
				imSize,
		conn.data(),
				3,
		connSize.data());

	for (int k = 0; k < range.size(); ++k)
		for (int i = 0; i < H.size(); ++i)
			for (int j = 0; j < H[0].size(); ++j){

				BW[k*H.size()* H[0].size() + H[0].size()*i + j] *= 255;
			}

	// Suppress peaks lower than a given threshold
//	accSlices.clear();
	for (int k = 0; k < range.size(); ++k){
		for (int j = 0; j < H[0].size(); ++j)
			for (int i = 0; i < H.size(); ++i)
				if (BW[k*H.size()* H[0].size() + H.size()*j + i] > 0 && H[i][j][k] >= mv)
					BW[k*H.size()* H[0].size() + H.size()*j + i] = 255;
				else
					BW[k*H.size()* H[0].size() + H.size()*j + i] = 0;
//		accSlices.push_back (Mat(H[0].size(), H.size(), CV_8U, BW.data()+k*H.size()* H[0].size()).t());
	}

	// Get maxima indexes
	vector<Point4i> maxima;
	for (int k = 0; k < range.size(); ++k)
		for (int j = 0; j < H[0].size(); ++j)
			for (int i = 0; i < H.size(); ++i)
				if (BW[k*H.size()* H[0].size() + H.size()*j + i] > 0 && H[i][j][k] > 0) // Val > 0 to avoid NULL maxima!
					maxima.push_back(Point4i(i,j,k, H[i][j][k]));

	// Sort maxima indexes
	std::sort (maxima.begin(), maxima.end(), ComparePoints);
	*/
	// Get first n maxima
	//for (int i = 0; i < std::min (npeaks, (int ) maxima.size()); ++i)
	//	peaks.push_back(Vec3i(maxima[i].i - margin, maxima[i].j - margin, range[maxima[i].k]));

	// Get all maxima with >= 80% max peak
	for (int i = 0; i < maxima.size(); ++i) {
		if (maxima[i].val >= 0.8 * maxima[0].val && peaks.size() < npeaks) {
			int y = maxima[i].i - margin;
			int x = maxima[i].j - margin;
			peaks.push_back(cv::Vec3i(maxima[i].i - margin, maxima[i].j - margin, range[maxima[i].k]));
		}
		else
			break;
	}


	return peaks;
}

// circlepoints
std::vector<cv::Point2i>
NewHough::circlepoints(int r)
{
	// Get number of rows needed to cover 1/8 of the circle
	int l = floor(r / sqrt(2) + 0.5);
	if (floor(sqrt(r * r - l * l) + 0.5) < l)   // % if crosses diagonal
		l = l - 1;

	// generate coords for 1/8 of the circle, a dot on each row
	std::vector<int> x0(l + 1);
	std::vector<int> y0(l + 1);
	for (int i = 0; i <= l; ++i) {
		x0[i] = i;
		y0[i] = floor(sqrt(r * r - x0[i] * x0[i]) + 0.5);
	}

	// Check for overlap
	int l2;
	if (y0[y0.size() - 1] == l)
		l2 = l;
	else
		l2 = l + 1;

	// Assemble first quadrant
	std::vector<int> x = x0;
	for (int i = l2 - 1; i > 0; --i)
		x.push_back(y0[i]);
	std::vector<int> y = y0;
	for (int i = l2 - 1; i > 0; --i)
		y.push_back(x0[i]);

	// Add next quadrant
	x0 = x;
	for (int i = 0; i < y.size(); ++i)
		x0.push_back(y[i]);
	y0 = y;
	for (int i = 0; i < x.size(); ++i)
		y0.push_back(-x[i]);

	// Assemble full circle
	x = x0;
	for (int i = 0; i < x0.size(); ++i)
		x.push_back(-x0[i]);
	y = y0;
	for (int i = 0; i < y0.size(); ++i)
		y.push_back(-y0[i]);

	std::vector<cv::Point2i> points(x.size());
	for (int i = 0; i < x.size(); ++i)
		points[i] = cv::Point2i(x[i], y[i]);
	//	points[i] = Point2i(y[i],x[i]);

	return points;
}

std::vector<std::vector<cv::Point3i>>
NewHough::getLUT(std::vector<int> range)
{
	std::vector<cv::Point3i> deg0, deg45, deg90, deg135, deg180, deg225, deg270, deg315;
	std::vector<std::vector<cv::Point3i>> table(8 * range.size());
	for (int i = 0; i < range.size(); ++i) {
		deg0.clear();
		deg45.clear();
		deg90.clear();
		deg135.clear();
		deg180.clear();
		deg225.clear();
		deg270.clear();
		deg315.clear();
		// Get all template points
		std::vector<cv::Point2i> points = circlepoints(range[i]);
		// Compute inner product between each point and each gradient direction and populate the point vectors
		for (int j = 0; j < points.size(); ++j) {
			float rx = points[j].x;
			float ry = points[j].y;

			double gx = 1;
			double gy = 0;

			double prod = rx * gy + ry * gx;
			if (prod < 0)
				deg0.push_back(cv::Point3i(points[j].x, points[j].y, i));

			gx = 0.71;
			gy = 0.71;
			prod = rx * gy + ry * gx;
			if (prod < 0)
				deg45.push_back(cv::Point3i(points[j].x, points[j].y, i));

			gx = 0;
			gy = 1;
			prod = rx * gy + ry * gx;
			if (prod < 0)
				deg90.push_back(cv::Point3i(points[j].x, points[j].y, i));

			gx = -0.71;
			gy = 0.71;
			prod = rx * gy + ry * gx;
			if (prod < 0)
				deg135.push_back(cv::Point3i(points[j].x, points[j].y, i));

			gx = -1;
			gy = 0;
			prod = rx * gy + ry * gx;
			if (prod < 0)
				deg180.push_back(cv::Point3i(points[j].x, points[j].y, i));

			gx = -0.71;
			gy = -0.71;
			prod = rx * gy + ry * gx;
			if (prod < 0)
				deg225.push_back(cv::Point3i(points[j].x, points[j].y, i));

			gx = 0;
			gy = -1;
			prod = rx * gy + ry * gx;
			if (prod < 0)
				deg270.push_back(cv::Point3i(points[j].x, points[j].y, i));

			gx = 0.71;
			gy = -0.71;
			prod = rx * gy + ry * gx;
			if (prod < 0)
				deg315.push_back(cv::Point3i(points[j].x, points[j].y, i));
		}

		table[8 * (range[i] - 1) + 0] = deg0;
		table[8 * (range[i] - 1) + 1] = deg45;
		table[8 * (range[i] - 1) + 2] = deg90;
		table[8 * (range[i] - 1) + 3] = deg135;
		table[8 * (range[i] - 1) + 4] = deg180;
		table[8 * (range[i] - 1) + 5] = deg225;
		table[8 * (range[i] - 1) + 6] = deg270;
		table[8 * (range[i] - 1) + 7] = deg315;
	}

	return table;
}

// getEllipseLUT
std::vector<std::vector<TPoint5i>>
NewHough::getEllipseLUT(std::vector<int> range)
{
	std::vector<TPoint5i> deg0, deg45, deg90, deg135, deg180, deg225, deg270, deg315;
	std::vector<std::vector<TPoint5i>> table(range.size() * range.size() * 6 * 8);

	for (int i = 0; i < range.size(); ++i)
		for (int j = 0; j <= i; ++j)
			for (int k = 0; k < 6; ++k) {
				// Generate template points
				std::vector<cv::Point> points;
				ellipse2Poly(cv::Point(0, 0), cv::Size(range[i], range[j]), 30 * k, 0, 360, 1, points);
				deg0.clear();
				deg45.clear();
				deg90.clear();
				deg135.clear();
				deg180.clear();
				deg225.clear();
				deg270.clear();
				deg315.clear();
					

				// Compute inner product between each point and each gradient direction and populate the point vectors
				for (int l = 0; l < points.size(); ++l) {
					TPoint5i pt;
					pt.x = points[l].x;
					pt.y = points[l].y;
					pt.z = range[i];
					pt.w = range[j];
					pt.v = k;

					float ry = points[l].x;
					float rx = points[l].y;

					double gx = 1;
					double gy = 0;

					double prod = rx * gy + ry * gx;
					if (prod < 0)
						deg0.push_back(pt);

					gx = 0.71;
					gy = 0.71;
					prod = rx * gy + ry * gx;
					if (prod < 0)
						deg45.push_back(pt);

					gx = 0;
					gy = 1;
					prod = rx * gy + ry * gx;
					if (prod < 0)
						deg90.push_back(pt);

					gx = -0.71;
					gy = 0.71;
					prod = rx * gy + ry * gx;
					if (prod < 0)
						deg135.push_back(pt);

					gx = -1;
					gy = 0;
					prod = rx * gy + ry * gx;
					if (prod < 0)
						deg180.push_back(pt);

					gx = -0.71;
					gy = -0.71;
					prod = rx * gy + ry * gx;
					if (prod < 0)
						deg225.push_back(pt);

					gx = 0;
					gy = -1;
					prod = rx * gy + ry * gx;
					if (prod < 0)
						deg270.push_back(pt);

					gx = 0.71;
					gy = -0.71;
					prod = rx * gy + ry * gx;
					if (prod < 0)
						deg315.push_back(pt);
				}
				table[range.size() * 6 * 8 * (range[i] - 1) + 6 * 8 * (range[j] - 1) + 8 * k + 0] = deg0;
				table[range.size() * 6 * 8 * (range[i] - 1) + 6 * 8 * (range[j] - 1) + 8 * k + 1] = deg45;
				table[range.size() * 6 * 8 * (range[i] - 1) + 6 * 8 * (range[j] - 1) + 8 * k + 2] = deg90;
				table[range.size() * 6 * 8 * (range[i] - 1) + 6 * 8 * (range[j] - 1) + 8 * k + 3] = deg135;
				table[range.size() * 6 * 8 * (range[i] - 1) + 6 * 8 * (range[j] - 1) + 8 * k + 4] = deg180;
				table[range.size() * 6 * 8 * (range[i] - 1) + 6 * 8 * (range[j] - 1) + 8 * k + 5] = deg225;
				table[range.size() * 6 * 8 * (range[i] - 1) + 6 * 8 * (range[j] - 1) + 8 * k + 6] = deg270;
				table[range.size() * 6 * 8 * (range[i] - 1) + 6 * 8 * (range[j] - 1) + 8 * k + 7] = deg315;
			}

	return table;
}

// Finds the intersection of two lines, or returns false.
// The lines are defined by (o1, p1) and (o2, p2).
bool
intersection(cv::Point2f o1,
						 cv::Point2f p1,
						 cv::Point2f o2,
						 cv::Point2f p2,
						 cv::Point2f &r)
{
	cv::Point2f x = o2 - o1;
	cv::Point2f d1 = p1 - o1;
	cv::Point2f d2 = p2 - o2;

	float cross = d1.x*d2.y - d1.y*d2.x;
	if (abs(cross) < /*EPS*/1e-8)
		return false;

	double t1 = (x.x * d2.y - x.y * d2.x) / cross;
	r = o1 + d1 * t1;
	return true;
}

std::vector<NewHough::EllipseHough>
NewHough::ellipse_hough(cv::Mat edges,
												cv::Mat bw,
												std::vector<int> range,
												cv::Mat GX,
												cv::Mat GY,
												cv::Mat mag,
												bool same,
												bool normalise,
												int npeaks)
{
	// Get indices of the edge coordinates
	std::vector<cv::Point2i> idx;
	unsigned char *rowPtr;
	for (int i = 0; i < edges.rows; ++i) {
		rowPtr = edges.ptr<unsigned char>(i);
		for (int j = 0; j < edges.cols; ++j)
			if (rowPtr[j] > 0)
				idx.push_back(cv::Point2i(i, j));
	}

	// Return no circles if edge not found!
	if (idx.size() < 1)
		return std::vector<NewHough::EllipseHough>();

	// Else edges have been found!

	// Quantize gradient along edges
	std::vector<int> qGrad(idx.size());
	for (int i = 0; i < idx.size(); ++i) {
		int dir;
		if (GY.at<float>(idx[i].x, idx[i].y) < 0)
			dir = (360 + atan2(GY.at<float>(idx[i].x, idx[i].y), GX.at<float>(idx[i].x, idx[i].y)) * 180 / PI) / 45.0;
		else
			dir = (atan2(GY.at<float>(idx[i].x, idx[i].y), GX.at<float>(idx[i].x, idx[i].y)) * 180 / PI) / 45.0;
		qGrad[i] = dir;
	}


	//////////
	// TEST //
	//////////

	// Threshold eye bw image
	cv::Mat eyeTH = cv::Mat(bw.rows, bw.cols, CV_8UC1);
	threshold(255 - bw, eyeTH, 0, 255, CV_THRESH_BINARY + cv::THRESH_OTSU);
	// Fill holes
	std::vector<std::vector<cv::Point>> contours;
	findContours(eyeTH, contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE);
	if (contours.size() > 0)
		for (int i = 0; i < contours.size(); ++i)
			drawContours(eyeTH, contours, i, 255, CV_FILLED);

	cv::Mat accu = cv::Mat::zeros(edges.rows, edges.cols, CV_32F);
	std::vector<std::vector<int>> accu2(edges.rows, std::vector<int>(edges.cols, 0));

	for (int i = 0; i < idx.size(); ++i) {
		cv::Mat temp = cv::Mat::zeros(edges.rows, edges.cols, CV_8U);
		float gx = -GX.at<float>(idx[i].x, idx[i].y);
		float gy = -GY.at<float>(idx[i].x, idx[i].y);
		float m = mag.at<float>(idx[i].x, idx[i].y);
		float gxx = gx / m;
		float gyy = gy / m;

		//line(temp, Point(idx[i].y, idx[i].x), Point(idx[i].y + 100*gxx, idx[i].x + 100*gyy), 1);
		
		line(temp,
				 cv::Point(idx[i].y, idx[i].x),
				 cv::Point(idx[i].y + range[range.size() - 1] * gxx,
				 idx[i].x + range[range.size() - 1] * gyy), 1);

		accumulate(temp, accu);
	}

	for (int i = 0; i < idx.size(); ++i) {
		float gx = -GX.at<float>(idx[i].x, idx[i].y);
		float gy = -GY.at<float>(idx[i].x, idx[i].y);
		float m = mag.at<float>(idx[i].x, idx[i].y);
		float gxx = gx / m;
		float gyy = gy / m;
		for (int j = 0; j < idx.size(); ++j)
			if (i != j) {
				float gxj = -GX.at<float>(idx[j].x, idx[j].y);
				float gyj = -GY.at<float>(idx[j].x, idx[j].y);
				float mj = mag.at<float>(idx[j].x, idx[j].y);
				float gxxj = gxj / mj;
				float gyyj = gyj / mj;
				cv::Point2f r;
				
				//bool inters = intersection(Point2f(idx[i].y, idx[i].x),Point2f(idx[i].y + 100*gxx, idx[i].x + 100*gyy), Point2f(idx[j].y, idx[j].x),Point2f(idx[j].y + 100*gxxj, idx[j].x + 100*gyyj), r);
				
				bool inters = intersection(
						cv::Point2f(idx[i].y, idx[i].x),
						cv::Point2f(idx[i].y + range[range.size() - 1] * gxx, idx[i].x + range[range.size() - 1] * gyy),
						cv::Point2f(idx[j].y, idx[j].x),
						cv::Point2f(idx[j].y + range[range.size() - 1] * gxxj, idx[j].x + range[range.size() - 1] * gyyj),
						r);

				if (inters && r.x >= 0 && r.x < edges.cols && r.y > 0 && r.y < edges.rows)
					accu2[r.y][r.x] += 1;
			}
	}

	cv::Mat accu2m = cv::Mat::zeros(edges.rows, edges.cols, CV_32F);
	for (int i = 0; i < edges.rows; ++i)
		for (int j = 0; j < edges.cols; ++j)
			accu2m.at<float>(i, j) = accu2[i][j];

	// Smooth accumulator
	//GaussianBlur(accu, accu, Size(3,3), 2);

	std::ofstream tmp("acc.dat");
	for (int i = 0; i < accu.rows; ++i) {
		for (int j = 0; j < accu.cols; ++j)
			tmp << accu2[i][j] << " ";
		tmp << std::endl;
	}
	tmp.close();

	// Select point pairs and estimate compatible ellipses
	std::vector<cv::Point> candidates;
	std::vector<EllipseHough> ellipses;
	double minVal;
	double maxVal;
	cv::Point minLoc;
	cv::Point maxLoc;
	minMaxLoc(accu2m, &minVal, &maxVal, &minLoc, &maxLoc);
	const float THRESH = 0.8;
	for (int i = 0; i < accu2m.rows; ++i)
		for (int j = 0; j < accu2m.cols; ++j)
			if (accu2m.at<float>(i, j) >= THRESH * maxVal)
				candidates.push_back(cv::Point(j, i));
	cv::Mat mask;
	cv::Mat bwMask;
	double minDens = 32768;
	EllipseHough bestEll;
	while (candidates.size() > 0) {
		// extract candidate point
		cv::Point2f p1 = candidates.back();


		for (int i = 0; i < candidates.size(); ++i) {
			// Check every possible pair
			cv::Point p2 = candidates[i];

			// Compute ellipse center
			cv::Point ct((p1.x + p2.x) / 2, (p1.y + p2.y) / 2);
			// Compute ellipse angle
			float angle = atan2(p2.y - p1.y, p2.x - p1.x);
			// Compute new coordinates respect to the elllipse reference system
			cv::Point p1e((p1.x - ct.x)*cos(angle) + (p1.y - ct.y)*sin(angle), (p1.x - ct.x)*sin(angle) - (p1.y - ct.y)*cos(angle));
			cv::Point p2e((p2.x - ct.x)*cos(angle) + (p2.y - ct.y)*sin(angle), (p2.x - ct.x)*sin(angle) - (p2.y - ct.y)*cos(angle));
			// If p2e.x < p1e.x swap points
			if (p2e.x < p1e.x) {
				cv::Point t = p1e;
				p1e = p2e;
				p2e = t;
			}
			// Find the best ellipse among all compatible ellipses within range
			for (int j = range[0]; j < range[range.size() - 1]; ++j) {

				// Found compatible ellipse
				int b = j;
				int a = (2 * p2e.x + sqrt(p2e.x*p2e.x + 4 * b*b)) / 2;
				int a2 = (2 * p2e.x - sqrt(p2e.x*p2e.x + 4 * b*b)) / 2;

				if (b <= a && a <= range[range.size() - 1]) {
					ellipses.push_back(EllipseHough(ct.x, ct.y, a, b, angle));
					// Create elliptical mask and compute density
					mask = cv::Mat::zeros(edges.rows, edges.cols, CV_8U);
					bwMask = cv::Mat::zeros(edges.rows, edges.cols, CV_8U);
					ellipse(mask, cv::Point(ct.x, ct.y), cv::Size(a, b), angle * 180 / CV_PI, 0, 360, 255, -1);
					bw.copyTo(bwMask, mask);
					cv::Scalar sm = sum(bwMask);
					cv::Scalar area = sum(mask);
					double dens = sm[0] / (area[0]);
					if (dens < minDens) {
						minDens = dens;
						bestEll = EllipseHough(ct.x, ct.y, a, b, angle);
					}
					//ellipse(bw, Point(ct.x, ct.y), Size(a, b), angle*180/CV_PI, 0, 360, 128);
					//circle(bw, Point(ct.x, ct.y),0,128);

				}

			}
		}
		candidates.pop_back();
	}
	//ellipse(edges, Point(bestEll.xc, bestEll.yc), Size(bestEll.a, bestEll.b), bestEll.rho*180/CV_PI, 0, 360, 128);
	//circle(edges, Point(bestEll.xc, bestEll.yc),0,128);


	//////////////
	// END TEST //
	//////////////



/*
	// Set up accumulator array, with a margin to avoid need for bounds checking
		int nr = edges.rows;
		int nc = edges.cols;
	 // int nradii = range.size();
		int margin = ceil(*(std::max_element(range.begin(), range.end()))) + 2;
		int nrh = nr + 2*margin + 1;        // increase size of accumulator
		int nch = nc + 2*margin + 1;
		vector<float> acc = vector<float> (nrh * nch * range.size() * range.size() * 6);

	// Loop over features
		for (int f = 0; f < idx.size(); ++f)
			for (int i = 0; i < range.size(); ++i)
				for (int j = 0; j <= i; ++j)
					for (int k = 0; k < 6; ++k){
			// Get all compatible points for the gradient direction

						vector<TPoint5i>& points = lutEllipse[100 * 8*6*(range[i] - 1) +8*6*(range[j] -1) + 8*k + qGrad[f]];
			for (int p = 0; p < points.size(); ++p)
				acc[nrh*nch*6*range.size() * i + nrh*nch*6*j + nrh*nch*k + nch*(points[p].y + idx[f].x + margin) + (points[p].x+idx[f].y + margin)] += 1;
			}



		if (same){
				// Remove peaks in the margin area
				for (int a = 0; a < range.size(); ++a)
			for (int b = 0; b < range.size(); ++b)
				for (int k = 0; k < 6; ++k)
						for (int i = 0; i < margin+1; ++i)
								for (int j = 0; j < nrh; ++j)
										acc[nrh*nch*6*range.size() * a + nrh*nch*6*b + nrh*nch*k + nch*i + j] = 0;

				for (int a = 0; a < range.size(); ++a)
			for (int b = 0; b < range.size(); ++b)
				for (int k = 0; k < 6; ++k)
						for (int i = nrh - margin+1; i < nrh; ++i)
								for (int j = 0; j < nch; ++j)
										acc[nrh*nch*6*range.size() * a + nrh*nch*6*b + nrh*nch*k + nchstd::*i + j] = 0;

				for (int a = 0; a < range.size(); ++a)
			for (int b = 0; b < range.size(); ++b)
				for (int k = 0; k < 6; ++k)
						for (int i = 0; i < nrh; ++i)
								for (int j = 0; j < margin+1; ++j)
										acc[nrh*nch*6*range.size() * a + nrh*nch*6*b + nrh*nch*k + nch*i + j] = 0;

				 for (int a = 0; a < range.size(); ++a)
			for (int b = 0; b < range.size(); ++b)
				for (int k = 0; k < 6; ++k)
						for (int i = 0; i < nrh; ++i)
								for (int j = nch - margin; j < nch; ++j)
										acc[nrh*nch*6*range.size() * a + nrh*nch*6*b + nrh*nch*k + nch*i + j] = 0;
		}

		if (normalise)
			 for (int a = 0; a < range.size(); ++a)
			for (int b = 0; b < range.size(); ++b)
				for (int k = 0; k < 6; ++k)
						for (int i = 0; i < nrh; ++i)
								for (int j = 0; j < nch; ++j)
										acc[nrh*nch*6*range.size() * a + nrh*nch*6*b + nrh*nch*k + nch*i + j] /= range[a];

		return ellipse_houghpeaks(acc, nrh, nch, range, npeaks, margin);
	*/
	return std::vector<NewHough::EllipseHough>(1, bestEll);
}

std::vector<NewHough::EllipseHough>
NewHough::ellipse_houghpeaks(std::vector<float>& acc,
														 int nrh,
														 int nch,
														 std::vector<int> range,
														 int npeaks,
														 int margin)
{
	std::vector<EllipseHough> peaks;

	// Compute max value
	float mv = 0;
	std::vector<float>::iterator maxit = std::max_element(acc.begin(), acc.end());
	mv = *maxit;

	// Retain only the 50% of the max value
 // mv *= 0.5;

// Get all the accepted maxima
	std::vector<TPoint6m> maxima;
	for (int a = 0; a < range.size(); ++a)
		for (int b = 0; b < range.size(); ++b)
			for (int k = 0; k < 6; ++k)
				for (int i = 0; i < nrh; ++i)
					for (int j = 0; j < nch; ++j)
						if (acc[nrh*nch * 6 * range.size() * a + nrh * nch * 6 * b + nrh * nch*k + nch * i + j] > 0 && acc[nrh*nch * 6 * range.size() * a + nrh * nch * 6 * b + nrh * nch*k + nch * i + j] >= mv) // Val > 0 to avoid NULL maxima!
							maxima.push_back(TPoint6m(j, i, range[a], range[b], 30 * k, acc[nrh*nch * 6 * range.size() * a + nrh * nch * 6 * b + nrh * nch*k + nch * i + j]));

	// Sort maxima indexes
	std::sort(maxima.begin(), maxima.end(), ComparePoints2);

	// Get all maxima with >= 80% max peak
	for (int i = 0; i < maxima.size(); ++i) {
		if (maxima[i].u >= 0.8 * maxima[0].u && peaks.size() < npeaks) {
			peaks.push_back(EllipseHough(maxima[i].x - margin, maxima[i].y - margin, maxima[i].z, maxima[i].w, maxima[i].v));
		}
		else
			break;
	}


	return peaks;
}
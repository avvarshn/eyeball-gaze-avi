#ifndef ADAPTIVECANNY_H
#define ADAPTIVECANNY_H

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>


class AdaptiveCanny
{
public:
    
    /**
     * @brief Canny method with automatic thresholds computation
     * @param image to process
     * @return binary mask of estimated image edges
     */
    static cv::Mat adaptiveCanny(cv::Mat img, cv::Mat& GX, cv::Mat& GY, cv::Mat& mag, int& maxTH, int& minTH);

private:
    /**
     * @brief Method for automatic threshold computation
     * @param Image gradient
     * @param PercentOfPixelsNotEdges
     * @param ratio between low and high thresholds
     * @param lowThresh
     * @param highThresh
     */
    static void selectThresholds(cv::Mat magGrad, double PercentOfPixelsNotEdges, double ThresholdRatio, double& lowThresh, double& highThresh);

    /**
     * @brief Method for computing the smoothed gradient of a given BW image
     * @param Image to process
     * @param Gaussian filter standard deviation
     * @param Reference to the horizontal gradient matrix
     * @param Reference to the vertical gradient matrix
     */
    static void smoothGradient(cv::Mat img, double sigma, cv::Mat& GX, cv::Mat& GY);
};

#endif // NEWCANNY_H

#include <vector>

#include "trainer.h"

using namespace cv;

// addTrainvector
void Trainer::addTrainvector (std::vector<float> features, Trainer::POSITION position){
    dataSet.push_back(features);

    switch (position){
    case N:
        labelsH.push_back(0);
        labelsV.push_back(1.0);
        break;
    case S:
        labelsH.push_back(0);
        labelsV.push_back(-1.0);
        break;
    case W:
        labelsH.push_back(-1.0);
        labelsV.push_back(0);
        break;
    case E:
        labelsH.push_back(1.0);
        labelsV.push_back(0);
        break;
    case CE:
        labelsH.push_back(0);
        labelsV.push_back(0);
        break;
    case NE:
        labelsH.push_back(1.0);
        labelsV.push_back(1.0);
        break;
    case NW:
        labelsH.push_back(-1.0);
        labelsV.push_back(1.0);
        break;
    case SE:
        labelsH.push_back(1.0);
        labelsV.push_back(-1.0);
        break;
    case SW:
        labelsH.push_back(-1.0);
        labelsV.push_back(-1.0);
        break;
    }
}

// trainSVMModels
void Trainer::trainSVMModels(){
    // Create Mat from dataset
    Mat trainingDataMat(dataSet.size(), dataSet[0].size(), CV_32FC1);
    for (int i = 0; i < trainingDataMat.rows; ++i){
        float* rowPtr = trainingDataMat.ptr<float>(i);
        for (int j = 0; j < trainingDataMat.cols; ++j)
            rowPtr[j] = dataSet[i][j];
    }
    // Initialize data structures
    Mat labelsHMat(labelsH.size(), 1, CV_32FC1, labelsH.data());
    Mat labelsVMat(labelsV.size(), 1, CV_32FC1, labelsV.data());

    // Set up SVM's parameters
    SVMParams params;
    params.svm_type    = SVM::C_SVC;
    params.kernel_type = SVM::RBF;
   // params.term_crit   = TermCriteria(CV_TERMCRIT_ITER, 100, 1e-6);

    // Train the SVM
    modelSVMH.train(trainingDataMat, labelsHMat, Mat(), Mat(), params);
    modelSVMV.train(trainingDataMat, labelsVMat, Mat(), Mat(), params);
}

// saveSVMModels
void Trainer::saveSVMModels(std::string fileName){
    modelSVMH.save(("models/" + fileName + "H.svm").c_str());
    modelSVMV.save(("models/" + fileName + "V.svm").c_str());
}

// loadSVMModels
void Trainer::loadSVMModels(std::string fileName){
    modelSVMH.load(("models/" + fileName + "H.svm").c_str());
    modelSVMV.load(("models/" + fileName + "V.svm").c_str());
}

// predictSVMPosition
Trainer::POSITION Trainer::predictPositionSVM(std::vector<float> features, Trainer::MODEL type){
    Mat sampleMat (features.size(), 1, CV_32F, features.data());
    POSITION position;
    switch (type){
    case MODEL::HORIZONTAL:{
        int response = modelSVMH.predict(sampleMat);
        switch (response){
        case -1: position = W; break;
        case 1: position = E; break;
        case 0: position = CE; break;
        } }break;
    case MODEL::VERTICAL:{
        int response = modelSVMV.predict(sampleMat);
        switch (response){
        case -1: position = S; break;
        case 1: position = N; break;
        case 0: position = CE; break;
        } }break;
    case MODEL::BOTH:{
        int responseH = modelSVMH.predict(sampleMat);
        int responseV = modelSVMV.predict(sampleMat);
        switch (responseH){
        case -1:
            switch (responseV){
            case -1: position = SW; break;
            case 1: position = NW; break;
            case 0: position = W; break;
            } break;

        case 1:
            switch (responseV){
            case -1: position = SE; break;
            case 1: position = NE; break;
            case 0: position = E; break;
            } break;

        case 0:
            switch (responseV){
            case -1: position = S; break;
            case 1: position = N; break;
            case 0: position = CE; break;
            } break;
        } break;
    }
    }
    return position;
}

// trainRegModels
void Trainer::trainRegModels(){
    // Create Mat from dataset
    Mat trainingDataMat(dataSet.size(), dataSet[0].size(), CV_32FC1);
    for (int i = 0; i < trainingDataMat.rows; ++i){
        float* rowPtr = trainingDataMat.ptr<float>(i);
        for (int j = 0; j < trainingDataMat.cols; ++j)
            rowPtr[j] = dataSet[i][j];
    }
    // Initialize data structures
    Mat labelsHMat(labelsH.size(), 1, CV_32FC1, labelsH.data());
    Mat labelsVMat(labelsV.size(), 1, CV_32FC1, labelsV.data());

    // Set up SVM's parameters
    SVMParams params;
    params.svm_type    = SVM::NU_SVR;
    params.kernel_type = SVM::LINEAR;
    params.nu = 0.5;
    //params.term_crit   = TermCriteria(CV_TERMCRIT_ITER, 100, 1e-6);

    // Train the SVM
    modelRegH.train(trainingDataMat, labelsHMat, Mat(), Mat(), params);
    modelRegV.train(trainingDataMat, labelsVMat, Mat(), Mat(), params);
}

// saveRegModels
void Trainer::saveRegModels(std::string fileName){
    modelRegH.save(("models/" + fileName + "H.svm").c_str());
    modelRegV.save(("models/" + fileName + "V.svm").c_str());
}

// loadRegModels
void Trainer::loadRegModels(std::string fileName){
    modelRegH.load(("models/" + fileName + "H.svm").c_str());
    modelRegV.load(("models/" + fileName + "V.svm").c_str());
}

// predictRegPosition
Point2i Trainer::predictPositionReg(std::vector<float> features, Trainer::MODEL type){
    Mat sampleMat (features.size(), 1, CV_32F, features.data());
    Point2i position;

    switch (type){
    case MODEL::HORIZONTAL:
        position.x = modelRegH.predict(sampleMat);
        position.y = 0;
        break;
    case MODEL::VERTICAL:
        position.y = modelRegV.predict(sampleMat);
        position.x = 0;
        break;
    case MODEL::BOTH:
        position.x = modelRegH.predict(sampleMat);
        position.y = modelRegV.predict(sampleMat);
        break;
    }
    return position;
}

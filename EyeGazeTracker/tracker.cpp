#include "tracker.h"

bool compareRect(cv::Rect rect_one, cv::Rect rect_two) {
	return (rect_one.height < rect_two.height);
}

Tracker::Tracker(void) {
	this->isInitialized = false;

	this->detectFace = true;
	this->FLAG_PAUSE = false;
	this->keyCode = 0;

	this->CALIBRATE = 0;
	this->TAKE_SNAPSHOT = 0;
	this->IS_FIRST = 2;
	this->KALMAN = 1;
	this->IIR_FILTER = 0;
	this->RECORD = 0;
	this->EYECENTER1 = 1;
	this->eyeRadius_MM = 12.0;

	this->PAUSE_FLAG = false;
	this->maxDensity = false;
	this->binaryThresholding = false;

	this->margVarX = 10.0;;
	this->margCovXX = (float) 0.1;

	//float coefIIR_Z = 1.;		// IIR coef. for depth

	this->irisCenterDetectionMode = NewHough::QUANTIZED_GRADIENT;

	this->kalmanFilter = cv::KalmanFilter(6, 4, 0);

	float kArgs[] = { 949, 0, 666,
										0, 947, 363, 
										0, 0, 1}; 

	cv::Mat(3, 3, CV_32F, (char*) kArgs).copyTo(this->K);
}

void Tracker::drawHeadPoseAxis(cv::Mat& img, const cv::Mat& rot, float p_line) {
	/* Create matrix which store each axis line's starting and end points. */
	cv::Mat axisPoints = (cv::Mat_<float>(3, 4) << 0, p_line,
																								 0, 0, 0, 0, -p_line,
																								 0, 0, 0, 0, -p_line);
	/* Apply rotation to each axis's line, this is what changes their end coordinates to match user's head pose. */
	axisPoints = rot.rowRange(0, 2) * axisPoints;

	/* Axis center starts at top right corner of screen, offset points so coordinate lines become visible. */
	axisPoints.row(0) += HEAD_POS_AXIS_OFFSET;
	axisPoints.row(1) += HEAD_POS_AXIS_OFFSET;

	/* Create point which is center of axis, this is where lines all start being drawn from. */
	cv::Point axisCenter(axisPoints.at<float>(0, 0), axisPoints.at<float>(1, 0));

	line(img,																/* Where to display line. */
		axisCenter,														/* Starting coordinate point. */
		cv::Point(axisPoints.at<float>(0, 1),	/* Ending coordinate point. */
		axisPoints.at<float>(1, 1)),					/* TODO: Unsure about this... */
		this->CV_COLOR_RED,										/* Line color. */
		HEAD_POSE_AXIS_LINE_THICKNESS,				/* Line thickness. */
		HEAD_POSE_AXIS_LINE_TYPE);						/* Line Type. */

	line(img,
		axisCenter,
		cv::Point(axisPoints.at<float>(0, 2),
		axisPoints.at<float>(1, 2)),
		this->CV_COLOR_GREEN,
		HEAD_POSE_AXIS_LINE_THICKNESS,
		HEAD_POSE_AXIS_LINE_TYPE);
	
	line(img,
		axisCenter,
		cv::Point(axisPoints.at<float>(0, 3),
		axisPoints.at<float>(1, 3)),
		this->CV_COLOR_BLUE,
		HEAD_POSE_AXIS_LINE_THICKNESS,
		HEAD_POSE_AXIS_LINE_TYPE);
}

void Tracker::getDesktopResolution(int& horizontal, int& vertical) {
	RECT desktop;

	/* Get a handle to the desktop window. */
	const HWND hDesktop = GetDesktopWindow();

	/* Get the size of screen to the variable desktop. */
	GetWindowRect(hDesktop, &desktop);

	/* Top left coordinates are (0,0) bottom right coordinates are (horizontal, vertical). */
	horizontal = desktop.right;
	vertical = desktop.bottom;
}

void Tracker::initKalmanFilter(void) {
	this->kalmanFilter.transitionMatrix = (cv::Mat_<float>(6, 6) <<
		1, 0, 0, 0, 1, 0,
		0, 1, 0, 0, 0, 1,
		0, 0, 1, 0, 1, 0,
		0, 0, 0, 1, 0, 1,
		0, 0, 0, 0, 1, 0,
		0, 0, 0, 0, 0, 1);

	this->kalmanFilter.measurementMatrix = (cv::Mat_<float>(4, 6) <<
		1, 0, 0, 0, 0, 0,
		0, 1, 0, 0, 0, 0,
		0, 0, 1, 0, 0, 0,
		0, 0, 0, 1, 0, 0);

	/* Initializes a scaled identity matrix.
	*
	* @Param[0]	Matrix to init.
	* @Param[1]	Scalar to apply.
	*/
	cv::setIdentity(this->kalmanFilter.processNoiseCov, cv::Scalar::all(1e-5));

	this->kalmanFilter.processNoiseCov.at<float>(4, 4) = (float) this->kalmanFilter.processNoiseCov.at<float>(5, 5) = .01;

	setIdentity(this->kalmanFilter.measurementNoiseCov, cv::Scalar::all(margVarX));

	//KF.measurementNoiseCov.at<float>(0,2) = KF.measurementNoiseCov.at<float>(2,0) = KF.measurementNoiseCov.at<float>(1,3) = KF.measurementNoiseCov.at<float>(3,1) = margCovXX;

	/* Maybe for initialization? */
	setIdentity(this->kalmanFilter.errorCovPost);
}

void Tracker::handleKeyPress(int keyCode) {
	switch (keyCode) {
	case KEYCODE_PLUS:
		/* Increases x of eyeball center displacement. */
		P_R1_E.at<float>(0, 0)--;
		break;
	case KEYCODE_MINUS:
		/* Decrease x of eyeball center displacement. */
		P_R1_E.at<float>(0, 0)++;
		break;
	case KEYCODE_OPEN_BRACKET:
		/* Increases y of eyeball center displacement. */
		P_R1_E.at<float>(1, 0)++;
		break;
	case 93:
		/* Decreases y of eyeball center displacement. */
		P_R1_E.at<float>(1, 0)--;
		break;
	case KEYCODE_PERIOD:
		/* Increases z of eyeball center displacement. */
		--P_R1_E.at<float>(2, 0);
		break;
	case KEYCODE_COMMA:
		/* Decreases z of eyeball center displacement. */
		++P_R1_E.at<float>(2, 0);
		break;
	case KEYCODE_NUM_0:
		/* Increases eyeball radius. */
		++this->eyeRadius_MM;
		break;
	case KEYCODE_NUM_9:
		/* Decreases eyeball radius. */
		--this->eyeRadius_MM;
		break;
	case KEYCODE_LOW_APLH_P:
		/* Freezes processing. Very useful for visualization. */
		this->PAUSE_FLAG = true;
		break;
	case KEYCODE_LOW_APLH_O:
		/* Unfreezes processing. */
		this->PAUSE_FLAG = false;
		break;
	case KEYCODE_LOW_APLH_M:
		/* Record sequence of frames. */
		RECORD = NFRAMESTORECORD;
		recordFile.open("record.txt");
		break;
	case KEYCODE_LOW_APLH_Z:
		/* Starts/ends calibration phase. (Only opens/closes file). */
		if (CALIBRATE == 1)
			calibrationFile.close();
		else calibrationFile.open("calibration.txt");

		CALIBRATE = (CALIBRATE == 0 ? 1 : 0);
		break;
	case KEYCODE_LOW_APLH_X:
		/* Takes snapshot during calibration phase. */
		TAKE_SNAPSHOT = 1;
		break;
	default:
		break;
	}
}

//void Tracker::run(cv::VideoCapture videoCapture, bool record) {
void Tracker::run(bool record) {
	// -------------------------------------------------------------------------------------------------
	// Initialization
	// -------------------------------------------------------------------------------------------------
	this->initKalmanFilter();

	INTRAFACE::XXDescriptor xxd(4);
	this->faceAlignment = new INTRAFACE::FaceAlignment(FILE_DETECTION_MODEL, FILE_TRACKING_MODEL, &xxd);
	if (!this->faceAlignment->Initialized()) {
		std::cout << "Failed to load either <" << FILE_DETECTION_MODEL << "> or <" << FILE_TRACKING_MODEL << ">.\n";
		free(this->faceAlignment);
		return;
	}

	this->faceCascadeClassifier = new cv::CascadeClassifier();
	if (!this->faceCascadeClassifier->load(FILE_FACE_DETECTION_MODEL)) {
		std::cout << "Failed to load <" << FILE_FACE_DETECTION_MODEL << ">.\n";
		free(this->faceCascadeClassifier);
		return;
	}

	//std::ofstream out(outputName);

	//int videoFrameWidth = (int) videoCapture.get(CV_CAP_PROP_FRAME_WIDTH);
	//int videoFrameHeight = (int) videoCapture.get(CV_CAP_PROP_FRAME_HEIGHT);

	std::vector<int> compressionParams;
	compressionParams.push_back(CV_IMWRITE_PNG_COMPRESSION);
	compressionParams.push_back(9);

	// -------------------------------------------------------------------------------------------------
	// Window / Screen setup.
	// -------------------------------------------------------------------------------------------------
	/* Used by gaze point visualition. Grab dimensions outside loop, so we are not wasting cycles. */
	int screenWidth, screenHeight;
	this->getDesktopResolution(screenWidth, screenHeight);
	
	cv::namedWindow("Main", cv::WINDOW_NORMAL);
	//cv::namedWindow("LeftEye", CV_WINDOW_AUTOSIZE);
	//cv::namedWindow("RightEye", CV_WINDOW_AUTOSIZE);

	// -------------------------------------------------------------------------------------------------
	// Initialize and open RealSense 2 Camera.
	// -------------------------------------------------------------------------------------------------
	rs2::pipeline rs2_pipe;
	rs2_pipe.start();

	while (keyCode != KEYCODE_ESCAPE) {
		this->handleKeyPress(keyCode);

		/* Get new frame from camera. */
		rs2::frameset rs_data = rs2_pipe.wait_for_frames();

		/* Get color frame and convert to OpenCV Mat. */
		rs2::frame rs2_color_frame = rs_data.get_color_frame(); //rs2_color_map(rs_data.get_color_frame());
		int rs2_color_frame_width = rs2_color_frame.as<rs2::video_frame>().get_width();
		int rs2_color_frame_height = rs2_color_frame.as<rs2::video_frame>().get_height();

		cv::Mat rs2_color_frame_mat(cv::Size(rs2_color_frame_width, rs2_color_frame_height),
											CV_8UC3,
											(void*) rs2_color_frame.get_data(),
											cv::Mat::AUTO_STEP);

		/* If using Webcame, use this to grab next frame. */
		//videoCapture >> frameOrig;

		cv::Mat rs2_color_mat_copy = rs2_color_frame_mat.clone();
		if (rs2_color_mat_copy.rows == 0 || rs2_color_mat_copy.cols == 0) {
			std::cout << "ERROR: Camera, unable to grab frame.\n";
			break;
		}

		// -------------------------------------------------------------------------------------------------
		// Run face detection.
		// -------------------------------------------------------------------------------------------------
		if (detectFace) {
			std::vector<cv::Rect> faces;
			faceCascadeClassifier->detectMultiScale(rs2_color_mat_copy, faces, 1.2, 2, 0, cv::Size(50, 50));

			/* If no faces found, do nothing. */
			if (faces.empty()) {
				imshow("Main", rs2_color_mat_copy);
				continue;
			}
			/* If face was found, run face detection on largest face. If face detection was NOT OK, then break out, otherwise
			 * set detect face flag to false.
			 */
			if (faceAlignment->Detect(rs2_color_mat_copy,
																*max_element(faces.begin(), faces.end(), compareRect),
																X0,
																score) != INTRAFACE::IF_OK) {
				break;
			}
			detectFace = false;
		} else {
			/* Run facial feature tracking on detected face. If alignment of face is NOT okay then break out, otherwise
		   * copy over facial data.
			 */
			if (faceAlignment->Track(rs2_color_mat_copy, X0, X, score) != INTRAFACE::IF_OK)
				break;
			X.copyTo(X0);
		}

		if (score < VALID_FACE_DETECTION_SCORE) {
			continue;
			detectFace = true;
		}

		if (FLAG_PAUSE)
			continue;

		// -------------------------------------------------------------------------------------------------
		// Draw facial landmarks.
		// -------------------------------------------------------------------------------------------------
		for (int i = 0; i < X0.cols; i++)
			circle(rs2_color_mat_copy, cv::Point((int)X0.at<float>(0, i), (int)X0.at<float>(1, i)), 1, cv::Scalar(255, 0, 255), -1);
				
		/* Run head pose estimation. */
		INTRAFACE::HeadPose *hp = new INTRAFACE::HeadPose();
		faceAlignment->EstimateHeadPose(X0, *hp);

		/* Draw head pose estimation on to frame. */
		this->drawHeadPoseAxis(rs2_color_mat_copy, hp->rot, 50);

		///* TODO: Test 8-7-17 don't use this anymore. */
		//cv::Point El_o(X0.at<float>(0, 4), X0.at<float>(1, 4));
		///* Inner eyebrow point - the most stable. */
		//cv::Point El_i(X0.at<float>(0, 5), X0.at<float>(1, 5));

		////cv::Point El_o (X0.at<float>(0,28), X0.at<float>(1,28));	// left eye - outermost corner
		////cv::Point El_i (X0.at<float>(0,25), X0.at<float>(1,25));	// left eye - innermost corner

		///* Used for distance computation. */
		///* Left eye - outermost corner. */
		//cv::Point El_o_old(X0.at<float>(0, 28), X0.at<float>(1, 28));
		///* Left eye - innermost corner. */
		//cv::Point El_i_old(X0.at<float>(0, 25), X0.at<float>(1, 25));
		///* Right eye - outermost corner. */
		//cv::Point Er_o(X0.at<float>(0, 19), X0.at<float>(1, 19));

		//cv::Point Sl(X0.at<float>(0, 7), X0.at<float>(1, 7));

		//if (!KALMAN)
		//	continue;
		//		
		//if (IS_FIRST == 1) {
		//	this->kalmanFilter.statePost.at<float>(0, 0) = El_i.x;
		//	this->kalmanFilter.statePost.at<float>(1, 0) = El_i.y;
		//	this->kalmanFilter.statePost.at<float>(2, 0) = El_o.x;
		//	this->kalmanFilter.statePost.at<float>(3, 0) = El_o.y;
		//	this->kalmanFilter.statePost.at<float>(4, 0) = El_i.x - oldEl_i.x;
		//	this->kalmanFilter.statePost.at<float>(5, 0) = El_i.y - oldEl_i.y;
		//}
		//cv::Mat predState = this->kalmanFilter.predict();
		//cv::Mat currObservation = (cv::Mat_<float>(4, 1) << El_i.x, El_i.y, El_o.x, El_o.y);
		//cv::Mat filtData = this->kalmanFilter.measurementMatrix * this->kalmanFilter.correct(currObservation);
		//cv::Mat	innovData = filtData - currObservation;
		//cv::Mat mesPredCov = this->kalmanFilter.measurementMatrix * this->kalmanFilter.errorCovPre *
		//										 this->kalmanFilter.measurementMatrix.t() + this->kalmanFilter.measurementNoiseCov;

		//cv::Mat partGate = mesPredCov.inv() * innovData;
		//float gateSq = partGate.dot(innovData);

		//if (gateSq > MAX_GATE) {
		//	IS_FIRST = 2;
		//	frame = cv::Scalar(255, 255, 255);
		//}

		//if (IS_FIRST == 0) {
		//	El_i.x = filtData.at<float>(0);
		//	El_i.y = filtData.at<float>(1);
		//	El_o.x = filtData.at<float>(2);
		//	El_o.y = filtData.at<float>(3);
		//}
		//else --IS_FIRST;

		///* TOOD: Not sure if this is needed. */
		//if (El_i.x < 0)									El_i.x = 0;
		//if (El_i.x > videoFrameWidth)		El_i.x = videoFrameWidth - 1;
		//if (El_i.y < 0)									El_i.y = 0;
		//if (El_i.y > videoFrameHeight)	El_i.y = videoFrameHeight - 1;
		//if (El_o.x < 0)									El_o.x = 0;
		//if (El_o.x > videoFrameWidth)		El_o.x = videoFrameWidth  - 1;
		//if (El_o.y < 0)									El_o.y = 0;
		//if (El_o.y > videoFrameHeight)	El_o.y = videoFrameHeight - 1;
		//
		//oldEl_i = El_i;

		///* Set to 0 after first iteration. */
		//int RESET_IIR = 1;

		//if (IIR_FILTER) {
		//	if ((abs(El_i.x - lastEl_i.x) > MAX_EYE_DIST) || (abs(El_i.y - lastEl_i.y) > MAX_EYE_DIST))
		//		RESET_IIR = 1;

		//	if (RESET_IIR == 0)
		//		/* Filter anchor. */
		//		El_i = COEF_IIR_ANCHOR * El_i + (1. - COEF_IIR_ANCHOR) * lastEl_i;

		//	lastEl_i = El_i;
		//}
		//cv::Mat P_El_o_c, P_El_o_old_c, P_Er_o_c, P_El_0_c;

		///* Compute distance. */
		//cv::Mat P_El_o_s = (cv::Mat_<float>(3, 1) << El_o.x, El_o.y, 1);
		//cv::Mat P_El_o_old_s = (cv::Mat_<float>(3, 1) << El_o_old.x, El_o_old.y, 1);
		//solve(K, P_El_o_old_s, P_El_o_old_c);
		//solve(K, P_El_o_s, P_El_o_c);
		//cv::Mat P_Er_o_s = (cv::Mat_<float>(3, 1) << Er_o.x, Er_o.y, 1);
		//solve(K, P_Er_o_s, P_Er_o_c);

		//cv::Mat D_ec2 = (cv::Mat_<float>(3, 1) << D_EC12, 0, 0);
		//D_ec2 = hp->rot * D_ec2;

		//float a = P_El_o_old_c.dot(P_El_o_old_c);
		//float b = P_Er_o_c.dot(P_Er_o_c);
		//float c = P_El_o_old_c.dot(P_Er_o_c);
		//float d = P_El_o_old_c.dot(D_ec2);
		//float e = P_Er_o_c.dot(D_ec2);

		///* This is the distance to the outermost left eye corner. */
		//float dist2ec1 = (b*d - c * e) / (c*c - a * b);

		///* Filter distance. */
		//if (RESET_IIR == 0)
		//	dist2ec1 = coefIIR_Z * dist2ec1 + (1 - coefIIR_Z) * lastZ;
		//lastZ = dist2ec1;

		///* Location in space of outermost left eye corner.
		// * Not used..?
		// */
		//P_El_o_c *= dist2ec1;

		///* 7-3-17 Toshi. */
		//cv::Mat P_El_i_s = (cv::Mat_<float>(3, 1) << El_i.x, El_i.y, 1);
		//cv::Mat P_El_i_c;
		//solve(K, P_El_i_s, P_El_i_c);

		///* Anchor point in space.*/
		//P_El_i_c = P_El_i_c * dist2ec1;

		///* Compute eye center. */
		//if (EYECENTER1)
		//	P_El_0_c = P_El_i_c + hp->rot * P_R1_E;
		//else P_El_0_c = (EYE_COEF_1 * P_El_i_c + (1. - EYE_COEF_1) * P_El_o_c) + hp->rot * P_R1_E;

		//cv::Mat P_El_0_s = K * P_El_0_c;

		//cv::Point2i eyeCenter((int) (P_El_0_s.at<float>(0, 0) / P_El_0_s.at<float>(2, 0)),
		//											(int) (P_El_0_s.at<float>(1, 0) / P_El_0_s.at<float>(2, 0)));

		//if (!fpu_error(eyeCenter.x) && !fpu_error(eyeCenter.y))
		//	circle(frame, eyeCenter, 3, cv::Scalar(0, 255, 0), -1);

		///* Print out head rotation. */
		//hp->rot.at<float>(0, 0);

		///* Extract eye features. */
		//std::vector<cv::Point> leftEyePoints, rightEyePoints;
		//int i;
		//for (i = 19; i <= 24; ++i)
		//	leftEyePoints.push_back(cv::Point(X0.at<float>(0, i), X0.at<float>(1, i)));

		//for (; i <= 30; ++i)
		//	rightEyePoints.push_back(cv::Point(X0.at<float>(0, i), X0.at<float>(1, i)));

		//cv::Rect leftBoundRect = cv::boundingRect(leftEyePoints);
		//cv::Rect rightBoundRect = cv::boundingRect(rightEyePoints);

		///* Augment bounding rects by ten pixels. */
		//leftBoundRect.x -= 5;
		//leftBoundRect.y -= 5;
		//leftBoundRect.width += 10;
		//leftBoundRect.height += 10;

		//rightBoundRect.x -= 5;
		//rightBoundRect.y -= 5;
		//rightBoundRect.width += 10;
		//rightBoundRect.height += 10;

		//cv::Mat leftEye, rightEye;
		//frameOrig(leftBoundRect).copyTo(leftEye);
		//frameOrig(rightBoundRect).copyTo(rightEye);

		//lEye = leftEye.clone();
		//rEye = rightEye.clone();

		/* Estimate eye centers. */
		//try {
		//	lCircle = leftGaze.computeIrisCenter(leftEye, K, dist2ec1, this->binaryThresholding, maxDensity,
		//																			 0, 0, this->irisCenterDetectionMode);

		//	circle(frame, cv::Point(lCircle[1] + leftBoundRect.x, lCircle[0] + leftBoundRect.y),
		//				 lCircle[2], cv::Scalar(0, 0, 255));
		//	
		//	circle(frame, cv::Point(lCircle[1] + leftBoundRect.x, lCircle[0] + leftBoundRect.y),
		//				 1, cv::Scalar(0, 0, 255));

		//	lEyeBW = leftGaze.getEqualizedEye();
		//	cvtColor(lEyeBW, leftEyeBW, CV_GRAY2BGR);

		//	rCircle = rightGaze.computeIrisCenter(rightEye, K, dist2ec1, this->binaryThresholding, maxDensity,
		//																				0, 0, this->irisCenterDetectionMode);

		//	rCircleDisp.x = rCircle[1] + rightBoundRect.x;
		//	rCircleDisp.y = rCircle[0] + rightBoundRect.y;

		//	if (IIR_FILTER) {
		//		if ((abs(rCircleDisp.x - rCircleDisp.x) > MAX_EYE_DIST) || (abs(rCircleDisp.y - rCircleDisp.y) > MAX_EYE_DIST))
		//			RESET_IIR = 1;
		//		if (RESET_IIR == 0) {
		//			// filter anchor
		//			rCircleDisp = COEF_IIR_ANCHOR * rCircleDisp + (1. - COEF_IIR_ANCHOR) * lastrCircleDisp;
		//		}
		//		lastrCircleDisp = rCircleDisp;
		//	}

		//	// RM 7-7-17
		//	float p_i[3] = { rCircle[1] + rightBoundRect.x, rCircle[0] + rightBoundRect.y,1. };
		//	cv::Mat P_i_s = cv::Mat(3, 1, CV_32F, (float *)p_i);
		//	cv::Mat P_i_c;
		//	solve(K, P_i_s, P_i_c);

		//	if ((CALIBRATE == 1) && (TAKE_SNAPSHOT == 1)) {
		//		// write direction vector to iris and eye center position. They should be parallel.
		//		TAKE_SNAPSHOT = 0;
		//		frame = cv::Scalar(255, 255, 255);
		//		for (int i = 0; i < 3; i++)
		//			calibrationFile << to_string(P_El_i_c.at<float>(i, 0)) << " ";
		//		calibrationFile << "\n";
		//		for (int i = 0; i < 3; i++)
		//			calibrationFile << to_string(P_El_o_c.at<float>(i, 0)) << " ";
		//		calibrationFile << "\n";
		//		for (int i = 0; i < 3; i++)
		//			calibrationFile << to_string(P_i_c.at<float>(i, 0)) << " ";
		//		calibrationFile << "\n";
		//			for (int i = 0; i < 3; i++)
		//			for (int j = 0; j < 3; j++)
		//				calibrationFile << to_string((float)hp->rot.at<float>(j, i)) << " ";
		//		calibrationFile << "\n\n";
		//	}

		//	/* Gaze visualiation. */
		//	cv::Mat backgroundScreen = cv::Mat(screenHeight, screenWidth, CV_8UC3);

		//	/* Compute where line towards iris center crosses eyeball. */
		//	float alpha = (P_i_c.dot(P_El_0_c) / P_i_c.dot(P_i_c)) *
		//		(1. - sqrt(1. - (P_i_c.dot(P_i_c) * (P_El_0_c.dot(P_El_0_c) -
		//			this->eyeRadius_MM * this->eyeRadius_MM)) /
		//			(P_i_c.dot(P_El_0_c)*P_i_c.dot(P_El_0_c))));
		//		P_i_c *= alpha;

		//	cv::Mat P_i_c_out = K * (6 * P_i_c - 5 * P_El_0_c);

		//	P_i_c_out.at<float>(0, 0) /= P_i_c_out.at<float>(2, 0);
		//	P_i_c_out.at<float>(1, 0) /= P_i_c_out.at<float>(2, 0);

		//	if (!fpu_error(p_i[0]) && !fpu_error(p_i[1]) && !fpu_error(P_i_c_out.at<float>(0, 0)) && !fpu_error((P_i_c_out.at<float>(1, 0))))
		//		line(frame, cv::Point(p_i[0], p_i[1]), cv::Point(P_i_c_out.at<float>(0, 0), P_i_c_out.at<float>(1, 0)), cv::Scalar(0, 0, 255));

		//	backgroundScreen = cv::Scalar(0, 0, 0);
		//	alpha = P_El_0_c.at<float>(0, 2) / (P_El_0_c.at<float>(0, 2) - P_i_c.at<float>(0, 2));

		//	cv::Mat P_g_c = P_El_0_c + alpha * (P_i_c - P_El_0_c);
		//	cv::Mat P_g_s = P_g_c.clone();

		//	cv::Mat P_g_s2 = P_i_c.clone();

		//	P_g_s.at<float>(0, 0) = 155. - P_g_s.at<float>(0, 0);	// Note that image looks flipped
		//	P_g_s.at<float>(1, 0) -= 8.;

		//	P_g_s2.at<float>(0, 0) = 155. - P_g_s2.at<float>(0, 0);
		//	P_g_s2.at<float>(1, 0) -= 8.;
		//		
		//	//				putText(backgroundScreen,"z_r0 = "+to_string((int)P_El_0_c.at<float>(2,0))+"z_i = "+to_string((int)P_i_c.at<float>(2,0)),Point (200, 70), FONT_HERSHEY_SIMPLEX, 0.3, 255);
		//	//				putText(backgroundScreen,"z_r0 = "+to_string((int)P_El_i_c.at<float>(2,0))+"z_i = "+to_string((int)P_El_o_c.at<float>(2,0)),Point (200, 70), FONT_HERSHEY_SIMPLEX, 0.3, 255);

		//	P_g_s = P_g_s * (float)screenWidth / PHYSICAL_SCREEN_WIDTH_MM;
		//	P_g_s2 = P_g_s2 * (float)screenWidth / PHYSICAL_SCREEN_WIDTH_MM;

		//	if (P_g_s.at<float>(0, 0) < 0)
		//		(P_g_s.at<float>(0, 0) = 0);
		//	else if (P_g_s.at<float>(0, 0) >= screenWidth)
		//		P_g_s.at<float>(0, 0) = screenWidth - 1;
		//	if (P_g_s.at<float>(1, 0) < 0)
		//		(P_g_s.at<float>(1, 0) = 0);
		//	else if (P_g_s.at<float>(1, 0) >= screenHeight)
		//		P_g_s.at<float>(1, 0) = screenHeight - 1;

		//	if (P_g_s2.at<float>(0, 0) < 0)
		//		(P_g_s2.at<float>(0, 0) = 0);
		//	else if (P_g_s2.at<float>(0, 0) >= screenWidth)
		//		P_g_s2.at<float>(0, 0) = screenWidth - 1;
		//	if (P_g_s2.at<float>(1, 0) < 0)
		//		(P_g_s2.at<float>(1, 0) = 0);
		//	else if (P_g_s2.at<float>(1, 0) >= screenHeight)
		//		P_g_s2.at<float>(1, 0) = screenHeight - 1;

		//	gpCircle2 = gpCircle1;
		//	gpCircle1 = gpCircle0;

		//	/* Show a circle at gaze point. Circle has radius equal to the distance to the gaze point in the previous frame. */
		//	if ((!fpu_error(P_g_s.at<float>(1, 0))) && (!fpu_error(P_g_s.at<float>(0, 0)))) {
		//		gpCircle0.x = (int)P_g_s.at<float>(0, 0); gpCircle0.y = (int)P_g_s.at<float>(1, 0);
		//		gpCircle0.z = sqrt((gpCircle0.x - gpCircle1.x)*(gpCircle0.x - gpCircle1.x) + (gpCircle0.y - gpCircle1.y)*(gpCircle0.y - gpCircle1.y));
		//		if (gpCircle0.z < MIN_GP_RADIUS)
		//			gpCircle0.z = MIN_GP_RADIUS;

		//		//circle(backgroundScreen, Point(gpCircle0.x,gpCircle0.y), 10,Scalar(0,255,255));
		//		circle(backgroundScreen, cv::Point(gpCircle0.x, gpCircle0.y), gpCircle0.z, cv::Scalar(0, 255, 255));
		//		circle(backgroundScreen, cv::Point(gpCircle1.x, gpCircle1.y), gpCircle1.z, cv::Scalar(0, 100, 100));
		//		circle(backgroundScreen, cv::Point(gpCircle2.x, gpCircle2.y), gpCircle2.z, cv::Scalar(0, 25, 25));

		//		imshow("TestScreen", backgroundScreen);
		//	}

		//	if (RESET_IIR == 1)
		//		RESET_IIR = 0;

		//	/* Write eyes initial positions and radiuses. */
		//	if (record)
		//		out << lCircle[1] << " " << lCircle[0] << " " << lCircle[2] << " " <<
		//		rCircle[1] << " " << rCircle[0] << " " << rCircle[2] << " ";

		//	/* Draw estimated irises (on face image). */
		//	circle(frame, cv::Point(rCircle[1] + rightBoundRect.x, rCircle[0] + rightBoundRect.y), rCircle[2], cv::Scalar(0, 0, 255));
		//	circle(frame, cv::Point(rCircle[1] + rightBoundRect.x, rCircle[0] + rightBoundRect.y), 1, cv::Scalar(0, 0, 255));

		//	rEyeBW = rightGaze.getEqualizedEye();
		//	cvtColor(rEyeBW, rightEyeBW, CV_GRAY2BGR);

		//	//cv::resize(d, frame(Rect(leftBoundRect.x - 2.5*leftEye.cols, leftBoundRect.y - 2.5*leftEye.rows, 3*leftBoundRect.width, 3*leftBoundRect.height)), Size());

		//	circle(leftEyeBW, cv::Point(lCircle[1], lCircle[0]), lCircle[2], cv::Scalar(0, 0, 255));
		//	circle(leftEyeBW, cv::Point(lCircle[1], lCircle[0]), 1, cv::Scalar(0, 0, 255));

		//	// test 7-4-17 - draw features
		//	for (int i = 0; i < X0.cols; i++)
		//		circle(leftEyeBW, cv::Point((int)X0.at<float>(0, i) - leftBoundRect.x, (int)X0.at<float>(1, i) - leftBoundRect.y), 1, cv::Scalar(0, 255, 0), -1);

		//	/* Draw estimated irises (on eye images). */
		//	cv::Mat tmp;

		//	cv::resize(leftEyeBW, tmp, cv::Size(3 * leftEye.cols, 3 * leftEye.rows));
		//	leftEyeBW = tmp.clone();
		//	//tmp.copyTo(frame(Rect(leftBoundRect.x - 2.5*leftEye.cols, leftBoundRect.y - 1*leftEye.rows, 3*leftBoundRect.width, 3*leftBoundRect.height)));
		//	circle(rightEyeBW, cv::Point(rCircle[1], rCircle[0]), rCircle[2], cv::Scalar(0, 0, 255));
		//	circle(rightEyeBW, cv::Point(rCircle[1], rCircle[0]), 1, cv::Scalar(0, 0, 255));
		//	// test 7-4-17 - draw features
		//	for (int i = 0; i < X0.cols; i++)
		//		circle(rightEyeBW, cv::Point((int)X0.at<float>(0, i) - rightBoundRect.x, (int)X0.at<float>(1, i) - rightBoundRect.y), 1, cv::Scalar(0, 255, 0), -1);
		//	circle(rightEyeBW, eyeCenter - cv::Point2i(rightBoundRect.x, rightBoundRect.y), 2, cv::Scalar(255, 0, 0), -1);

		//	cv::resize(rightEyeBW, tmp, cv::Size(3 * rightEye.cols, 3 * rightEye.rows));
		//	//tmp.copyTo(frame(Rect(rightBoundRect.x + 1*rightEye.cols, rightBoundRect.y - 1*rightEye.rows, 3*rightBoundRect.width, 3*rightBoundRect.height)));
		//	rightEyeBW = tmp.clone();

		//	//moveWindow("LeftEye", 110, 110);
		//	//moveWindow("RightEye", 110 + 4 * leftBoundRect.width, 110);
		//	//moveWindow("LeftEye", 400 - 3*leftBoundRect.width , 110);
		//	//moveWindow("RightEye", 450, 110);
		//}
		//catch (IrisDetectionException e) {
		//	std::cout << "\nERROR: IrisDetectionException\n";
		//}
		//catch (ImageTypeException e) {
		//	std::cout << "\nERROR: ImageTypeException\n";
		//}
		//catch (ImageSizeException e) {
		//	std::cout << "\nERROR: ImageSizeException\n";
		//}
		//catch (std::exception &e) {
		//	std::cout << "\nERROR: Some other type of std exception\n";
		//	std::cout << e.what() << "\n";
		//}

		cv::flip(rs2_color_mat_copy, rs2_color_mat_copy, 1);

		cv::imshow("Main", rs2_color_mat_copy);

		//cv::imshow("LeftEye", leftEyeBW);
		//cv::imshow("RightEye", rightEyeBW);
		
		keyCode = cv::waitKey(5);
	}
	
	cv::destroyAllWindows();
}
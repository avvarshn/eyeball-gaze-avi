#include "eyegaze.h"

cv::Vec3i EyeGaze::computeIrisCenter(cv::Mat eye, cv::Mat K,
																		 float depth,
																		 bool thresh, bool maxD,
																		 int sColor, int sSpace, int options) {
	/* Make sure matrix type is valid. */
	if (eye.type() != CV_8UC3 || eye.rows < 1 || eye.cols < 1)
		throw ImageTypeException();

	/* Sharpen image quality. */
	GaussianBlur(eye, this->equalizedEye, cv::Size(0, 0), 10);
	addWeighted(eye, 1.5, this->equalizedEye, -0.5, 0, eye);

	this->equalizedEye = eye.clone();

	cv::Mat eyeTH(this->equalizedEye.rows, this->equalizedEye.cols, CV_8UC1);
	this->eyeEdges = cv::Mat(this->equalizedEye.rows, this->equalizedEye.cols, CV_8UC1);
	
	cv::Mat GX, GY, mag;
	int maxTH, minTH;

	/* Filter RGB image if needed. */
	if (sColor > 0 && sSpace > 0) {
		cv::Mat tmp = this->equalizedEye.clone();
		bilateralFilter(tmp, this->equalizedEye, -1, sColor, sSpace);
	}

	/* Blur image to reduce noise. */
	//GaussianBlur(eye,eye, Size(3,3), 0, 0, BORDER_DEFAULT);

	/* Convert filtered/unfiltered image in gray scale, extracting luminance channel from the YUV converted BGR image. (/
	cv::cvtColor(this->equalizedEye, this->equalizedEye, CV_BGR2GRAY);
	// TODO: check if this conversion is different from the Matlab one and is dependant from the illumination!

	// Maximize contrast of the grayscale image
	cv::equalizeHist(this->equalizedEye, this->equalizedEye);

	// Threshold image with Otsu's method, if required (adaptive thresholding)
	if (thresh) {
		cv::threshold(this->equalizedEye, eyeTH, 0, 255, CV_THRESH_BINARY_INV + cv::THRESH_OTSU);
		// Fill holes
		std::vector<std::vector<cv::Point>> contours;
		findContours(eyeTH, contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE);
		if (contours.size() > 0)
			for (int i = 0; i < contours.size(); ++i)
				drawContours(eyeTH, contours, i, 255, CV_FILLED);
	}


	// Compute edges on the [not] thresholded grayscale image
	if (thresh)
		// Apply adaptive threshold Canny on the thresholded image
			// TODO: use non adaptive Canny in this case
		this->eyeEdges = AdaptiveCanny::adaptiveCanny(eyeTH, GX, GY, mag, maxTH, minTH);
	else
		// Canny on the not thresholded image
		this->eyeEdges = AdaptiveCanny::adaptiveCanny(this->equalizedEye, GX, GY, mag, maxTH, minTH);

	// Compute iris range [pxl] according to the eye depth ([10 14] mm, or [5 7] mm radius at DISTANCE meters
	// For a 1280x720 image; formula is r = R * sqrt(fx^2 + fy^2) / D with R radius [m], D depth [m] and fx, fy focal lenghts [pxl], r radius [pxl]

	// Find highest consensus circle with iris diameter
	float  multiplier = sqrt(K.at<float>(0, 0) * K.at<float>(0, 0) + K.at<float>(1, 1) * K.at<float>(1, 1));

	//int minRadPxl = MIN_IRIS_RADIUS * multiplier / depth;

	int minRadPxl = 0.8 * MIN_IRIS_RADIUS * multiplier / depth;
	int maxRadPxl = MAX_IRIS_RADIUS * multiplier / depth;
	//range = vector<int>  (maxRadPxl - minRadPxl + 1);

	range = std::vector<int>(std::min(10, maxRadPxl - minRadPxl + 1));

	int step = 1;
	if (range.size() == 10)
		step = floor((maxRadPxl - minRadPxl + 1) / 10);
	for (int i = 0; i < range.size(); ++i)
		range[i] = minRadPxl + i * step;

	//vector<Vec3f> circlesF;
	//Mat imgCopy = eye.clone();
	// GaussianBlur(imgCopy, imgCopy, Size(15,15), 2,2); // TODO: reduce filter size	
	//cv::HoughCircles(imgCopy, circlesF, CV_HOUGH_GRADIENT, 1, imgCopy.rows/8, 20, 10, 0, 0); 
	// Apply ordinary circle hough transform or maximum density variation according to the input options  
	cv::Vec3i circle;
	std::vector<cv::Vec3i> circles = this->hough.circle_hough(this->eyeEdges, range, true, true, 100, GX, GY, mag, options);

	// Select the hough circle
	if (circles.size() > 0) {
		if (maxD) {
			// Compute integral image of complement of the equalized eye BW image
			cv::Mat intImage;
			std::vector<unsigned char> white(this->equalizedEye.rows * this->equalizedEye.cols, 255);
			cv::Mat cplImage = cv::Mat(this->equalizedEye.rows, this->equalizedEye.cols, CV_8U, white.data()) - this->equalizedEye;
			// Otsu's thresholding after Gaussian filtering
			//Mat blurred;
			//GaussianBlur(cplImage, blurred, Size (5,5),2);
			//threshold(blurred, cplImage, 0,255,THRESH_BINARY+THRESH_OTSU);
			integral(cplImage, intImage);
			// Compute circle with the maximum density
			float maxDens = 0;
			int idx = 0;
			for (int k = 0; k < circles.size(); ++k) {
				int minX = floor(std::max<float>(1, circles[k][1] - circles[k][2]));
				int minY = floor(std::max<float>(1, circles[k][0] - circles[k][2]));
				int maxX = floor(std::min<float>(this->equalizedEye.cols - 1, circles[k][1] + circles[k][2]));
				int maxY = floor(std::min<float>(this->equalizedEye.rows - 1, circles[k][0] + circles[k][2]));
				float D = intImage.at<int>(maxY, maxX) + intImage.at<int>(minY, minX) - intImage.at<int>(minY, maxX) - intImage.at<int>(maxY, minX);
				//D = D / (circles[k][2] * circles[k][2]);
				D = D / ((maxX - minX)*(maxY - minY) * 255);// Max summed luminance is 255*rectangle area!
				if (D > maxDens) {
					maxDens = D;
					idx = k;
				}
			}
			circle = circles[idx];

		}
		else {
			circle = circles[0];
		}
	}

	return cv::Vec3i(circle[0], circle[1], circle[2]);
}

// computeHeadPosition
cv::Point3i EyeGaze::computeHeadPosition(cv::Mat R, cv::Mat K, cv::Point El, cv::Point Er, int H, int D) {
	int ul = El.x;
	int vl = El.y;
	int ur = Er.x;
	int vr = Er.y;
	double fx = K.at<float>(0, 0);
	double fy = K.at<float>(1, 1);
	int cx = K.at<float>(0, 2);
	int cy = K.at<float>(1, 2);

	//R = R';

	cv::Vec3f r1, r2, r3;
	r1[0] = R.at<float>(0, 0);
	r1[1] = R.at<float>(0, 1);
	r1[2] = R.at<float>(0, 2);
	r2[0] = R.at<float>(1, 0);
	r2[1] = R.at<float>(1, 1);
	r2[2] = R.at<float>(1, 2);
	r3[0] = R.at<float>(2, 0);
	r3[1] = R.at<float>(2, 1);
	r3[2] = R.at<float>(2, 2);

	cv::Point3f Olh(-D / 2.0, H, 0);
	cv::Point3f Orh(D / 2.0, H, 0);

	double a = ur - cx;
	double b = ul - cx;
	double c = vr - cy;
	double d = vl - cy;

	double e = r3[0] * (Orh.x - Olh.x) + r3[1] * (Orh.y - Olh.y) + r3[2] * (Orh.z - Olh.z);

	double A = a * a * fy * fy + c * c * fx * fx + fx * fx * fy * fy;
	double B = b * b * fy * fy + d * d * fx * fx + fx * fx * fy * fy;
	double C = -2 * a * b * fy * fy - 2 * c * d * fx * fx - 2 * fx * fx * fy * fy;
	double E = -D * D * fx * fx * fy * fy;

	double z1 = (-(2 * A + C)* e + sqrt((2 * A + C) * (2 * A + C) * e * e - 4 * (A + B + C) * E)) / (2 * (A + B + C));
	double z2 = (-(2 * A + C)* e - sqrt((2 * A + C) * (2 * A + C) * e * e - 4 * (A + B + C) * E)) / (2 * (A + B + C));

	// Select the correct solution (z > 0)
	double zl = 0;
	if (z1 > 0)
		zl = z1;
	else
		zl = z2;

	// Sobsitute solution to find the remaining coordinates
	double tx = floor(zl * (ul - cx) / fx - (r1[0] * Olh.x + r1[1] * Olh.y + r1[2] * Olh.z) + 0.5);
	double ty = floor(zl * (vl - cy) / fy - (r2[0] * Olh.x + r2[1] * Olh.y + r2[2] * Olh.z) + 0.5);
	double tz = floor(zl - (r3[0] * Olh.x + r3[1] * Olh.y + r3[2] * Olh.z) + 0.5);

	return cv::Point3i((int)tx, (int)ty, (int)tz);
}

cv::Point2i EyeGaze::projectOnPlane(cv::Mat R, cv::Point3i position, cv::Mat K, cv::Point point2D) {
	int u = point2D.x;
	int v = point2D.y;
	double fx = K.at<float>(0, 0);
	double fy = K.at<float>(1, 1);
	int cx = K.at<float>(0, 2);
	int cy = K.at<float>(1, 2);

	//R = R';

	cv::Vec3f r1, r2, r3;
	r1[0] = R.at<float>(0, 0);
	r1[1] = R.at<float>(0, 1);
	r1[2] = R.at<float>(0, 2);
	r2[0] = R.at<float>(1, 0);
	r2[1] = R.at<float>(1, 1);
	r2[2] = R.at<float>(1, 2);
	r3[0] = R.at<float>(2, 0);
	r3[1] = R.at<float>(2, 1);
	r3[2] = R.at<float>(2, 2);

	double tx = position.x;
	double ty = position.y;
	double tz = position.z;

	double a = (u - cx) / fx;
	double b = (v - cy) / fy;

	// Sobsitute solution to find the remaining coordinates
	double X = ((a * r3[1]) * (ty - b * tz) - (b * r3[1] - r2[1]) * (tx - a * tz)) / ((b * r3[0] - r2[0]) * (a * r3[1] - r1[1]) - (b * r3[1] - r2[1]) * (a * r3[0] - r1[0]));
	double Y = (tx - a * tz - (a * r3[0] - r1[0]) * X) / (a * r3[1] - r1[1]);

	return cv::Point2i((int)floor(X + 0.5), (int)floor(Y + 0.5));
}

/*
vector<EyeGaze::EllipseHough> findEllipses(Mat edges){
	// some global constants
	const float EL_COVERAGE_RATIO = 0.9;
	const int EL_VERIFICATION_DISTANCE = 1;
	const int EL_PATH_POINTS = 51;
	const int MIN_MINOR_FREQUENCY = 30;
	const int MIN_HALF_MAJOR = 8;
	const int MIN_HALF_MINOR = 6;

	// Extract pixel coordinates
	// Get indices of the edge coordinates
	vector<Point2i> pxy;
	unsigned char *rowPtr;
	for (int i = 0; i < edges.rows; ++i){
		rowPtr = edges.ptr<unsigned char>(i);
		for (int j = 0; j < edges.cols; ++j)
			if (rowPtr[j] > 0)
				pxy.push_back(Point2i(j,i));
	}

	// Return no circles if edge not found!
	if (pxy.size() < 1);
	//	return vector<Vec3i>();

	// Else edges have been found!
	vector<EyeGaze::EllipseHough> ellipses;

	// Applying Hough transform for ellipses detection.
	// Algorithm is taken from this paper:
	// http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.1.8792&rep=rep1&type=pdf

	int cIx = -1;
	//colors = [(255,0,0),(0,200,0),(0,0,255)]
	for (int i = 0; i < pxy.size(); ++i)
		for (int j = 0; j < pxy.size(); ++j){
			if (i == j)
				continue;
			map<int,int> bbins;
			float dist = sqrt((pxy[i].x - pxy[j].x)*(pxy[i].x - pxy[j].x) + (pxy[i].y - pxy[j].y)*(pxy[i].y - pxy[j].y));
			Point2i cent;
			float a, alfa, d, f;
			int b;
			double cost;
			if (dist >= 2 * MIN_HALF_MAJOR){
				cent = Point2i((pxy[i].x+pxy[j].x)/2.0,(pxy[i].y+pxy[j].y)/2.0);
				a = dist/2.0; // # semi-length of major axis
				alfa = atan2((pxy[j].y - pxy[i].y),(pxy[j].x - pxy[i].x));
				for (int k = 0; k < pxy.size(); ++k){// Finding best candidate for minor axes b
					d = sqrt((pxy[k].x - cent.x)*(pxy[k].x - cent.x) + (pxy[k].y - cent.y)*(pxy[k].y - cent.y));
					if (d >= MIN_HALF_MINOR){
						f = sqrt((pxy[k].x - pxy[j].x)*(pxy[k].x - pxy[j].x) + (pxy[k].y - pxy[j].y)*(pxy[k].y - pxy[j].y));
						cost = (a*a + d*d - f*f)/(0.00001+2.*a*d);
						double val = (a*a * d*d * (1.-cost*cost));
						if (val < 0)
							val = 0;
						b = sqrt(val/(0.00001 + a*a - d*d * cost*cost));  // semi-length of minor axis
						if (bbins.find(b) != bbins.end())
							bbins[b]+=1;
						else
							if (b > 0)
								bbins[b]=1;
					} // End if (d >= MIN_HALF_MINOR)
				} // End for k
				//// reverse map for computing frerequencies
				//map<int, int> bbins_rev;
				//for (map<int,int>::iterator it = bbins.begin(); it!=bbins.end(); ++it)
				//	bbins_rev[it->second] = it->first;

				// Compute max freq
				int max_freq = -1;
				int bmax;
				for (map<int,int>::iterator it = bbins.begin(); it!=bbins.end(); ++it)
					if (it->second > max_freq){
						max_freq = it->second;
						bmax = it->first;
					}
				//int bmax = bbins_rev[max_freq];
				// Did we found probable ellipse ?
				if (max_freq >= MIN_MINOR_FREQUENCY && alfa >=0.0 && bmax >= MIN_HALF_MINOR){
					// Generate ellipse
					vector<Point2i> elData (EL_PATH_POINTS);
					for (int e = 0; e < elData.size(); ++e){
						int elx, ely;
						elx = cent.x + a * cos(2.*CV_PI*e/float(EL_PATH_POINTS-1)) * cos(alfa) - bmax * sin(2.*CV_PI*e/float(EL_PATH_POINTS-1)) * sin(alfa);
						ely = cent.y + a * cos(2.*CV_PI*e/float(EL_PATH_POINTS-1)) * sin(alfa) + bmax * sin(2.*CV_PI*e/float(EL_PATH_POINTS-1)) * cos(alfa);
					}

					vector<Point2i> supported;
					float supportRatio = 0.0;
					// Counting how much pixels lies on ellipse path
					for (int m = 0; m < EL_PATH_POINTS; ++m){
						int elx, ely;
						elx = elData[m].x;
						ely = elData[m].y;
						bool added = false;
						for (int n = 0; n < pxy.size(); ++n){
							if (sqrt((elx - pxy[n].x)*(elx - pxy[n].x) + (ely - pxy[n].y)*(ely - pxy[n].y)) <= EL_VERIFICATION_DISTANCE)
								supported.push_back(pxy[n]);
							if (!added){
								supportRatio += 1./float(EL_PATH_POINTS);
								added = true;
							}
						}

						// Remove double supported points
						set<Point2i> temp (supported.begin(), supported.end());
						supported.clear();
						supported = vector<Point2i> (temp.begin(), temp.end());

						// if number of pixels on ellipse path is big enough
						if (supportRatio >= EL_COVERAGE_RATIO){
							cIx = (cIx+1)%3;
							//print "coverage %.2f" % supportRatio,"frequency ", max_freq, "center ", cent, "angle %.2f" % alfa, "axes (%.2f,%.2f)" % (a, bmax)
							// Removing founded ellipse pixels from further analysis
							while (supported.size() > 0){
								Point2i pt = supported.back();

								for  (vector<Point2i>::iterator it = pxy.begin(); it != pxy.end(); ++it)
									if (*it == pt){
										pxy.erase(it);
										break;
									}
									supported.pop_back();
							}

							// Add ellipse
							EyeGaze::EllipseHough ell;
							ell.xc = cent.x;
							ell.yc = cent.y;
							ell.a = a;
							ell.b = bmax;
							ell.rho = alfa;
							ellipses.push_back(ell);

							// Drawing founded ellipse
							//for i in range(EL_PATH_POINTS):
							//elx,ely = elData[i]
							//if i < EL_PATH_POINTS-1:
							//draw.line(elData[i] + elData[i+1], fill=colors[cIx])
						}  // End if (supportRatio >= EL_COVERAGE_RATIO)

					}
				} // End (max_freq >= MIN_MINOR_FREQUENCY && alfa >=0.0 && bmax >= MIN_HALF_MINOR)
			} // End if (dist >= 2 * MIN_HALF_MAJOR)
		}
		return ellipses;
		*/
}

// computeIrisCenter
NewHough::EllipseHough EyeGaze::computeIrisCenterEllipse(cv::Mat eye, cv::Mat K,
																												 float depth, bool thresh,
																												 bool maxD, int sColor, int sSpace) {
	// Check image type and size
	if (eye.type() != CV_8UC3)
		throw ImageTypeException();

	if (eye.rows < 1 || eye.cols < 1)
		throw ImageSizeException();

	// Try image sharpening
	cv::GaussianBlur(eye, this->equalizedEye, cv::Size(0, 0), 10);
	addWeighted(eye, 1.5, this->equalizedEye, -0.5, 0, this->equalizedEye);


	// Else image type and size are correct; pre-allocate matrices
	this->equalizedEye = eye.clone();
	cv::Mat eyeTH = cv::Mat(this->equalizedEye.rows, this->equalizedEye.cols, CV_8UC1);
	this->eyeEdges = cv::Mat(this->equalizedEye.rows, this->equalizedEye.cols, CV_8UC1);
	cv::Mat GX, GY, mag;
	int maxTH, minTH;

	// Filter RGB image if required
	if (sColor > 0 && sSpace > 0) {
		cv::Mat tmp = this->equalizedEye.clone();
		bilateralFilter(tmp, this->equalizedEye, -1, sColor, sSpace);
	}

	// Blur image to reduce noise
	//GaussianBlur(eye,eye, Size(3,3), 0, 0, BORDER_DEFAULT);

	// Convert filtered/unfiltered image in gray scale extracting the luminance channel from the YUV converted BGR image
	cv::cvtColor(this->equalizedEye, this->equalizedEye, CV_BGR2GRAY);

	// Maximize contrast of the grayscale image
	cv::equalizeHist(this->equalizedEye, this->equalizedEye);

	// Threshold image with Otsu's method, if required (adaptive thresholding)
	if (thresh) {
		threshold(255 - this->equalizedEye, eyeTH, 0, 255, CV_THRESH_BINARY + cv::THRESH_OTSU);
		// Fill holes
		std::vector<std::vector<cv::Point>> contours;
		findContours(eyeTH, contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE);
		if (contours.size() > 0)
			for (int i = 0; i < contours.size(); ++i)
				drawContours(eyeTH, contours, i, 255, CV_FILLED);
	}


	// Compute edges on the [not] thresholded grayscale image
	if (thresh)
		// Apply adaptive threshold Canny on the thresholded image
			// TODO: use non adaptive Canny in this case
		this->eyeEdges = AdaptiveCanny::adaptiveCanny(eyeTH, GX, GY, mag, maxTH, minTH);
	else
		// Canny on the not thresholded image
		this->eyeEdges = AdaptiveCanny::adaptiveCanny(this->equalizedEye, GX, GY, mag, maxTH, minTH);


	// Compute iris range [pxl] according to the eye depth ([10 14] mm, or [5 7] mm radius at DISTANCE meters
	// For a 1280x720 image; formula is r = R * sqrt(fx^2 + fy^2) / D with R radius [m], D depth [m] and fx, fy focal lenghts [pxl], r radius [pxl]

	// Find highest consensus circle with iris diameter
	float  multiplier = sqrt(K.at<float>(0, 0) * K.at<float>(0, 0) + K.at<float>(1, 1) * K.at<float>(1, 1));
	//int minRadPxl = MIN_IRIS_RADIUS * multiplier / depth;
	int minRadPxl = 0.8 * MIN_IRIS_RADIUS * multiplier / depth;
	int maxRadPxl = MAX_IRIS_RADIUS * multiplier / depth;
	//range = vector<int>  (maxRadPxl - minRadPxl + 1);
	range = std::vector<int>(std::min(10, maxRadPxl - minRadPxl + 1));
	int step = 1;
	if (range.size() == 10)
		step = floor((maxRadPxl - minRadPxl + 1) / 10);
	for (int i = 0; i < range.size(); ++i)
		range[i] = minRadPxl + i * step;

	NewHough::EllipseHough ell;
	std::vector<NewHough::EllipseHough> ellipses = this->hough.ellipse_hough(this->eyeEdges, this->equalizedEye, range, GX, GY, mag, true, true, 100);

	// Select the hough circle
	if (ellipses.size() > 0) {
		if (maxD) {
			// Compute integral image of complement of the equalized eye BW image
			cv::Mat intImage;
			std::vector<unsigned char> white(this->equalizedEye.rows * this->equalizedEye.cols, 255);
			cv::Mat cplImage = cv::Mat(this->equalizedEye.rows, this->equalizedEye.cols, CV_8U, white.data()) - this->equalizedEye;
			// Otsu's thresholding after Gaussian filtering
			//Mat blurred;
			//GaussianBlur(cplImage, blurred, Size (5,5),2);
			//threshold(blurred, cplImage, 0,255,THRESH_BINARY+THRESH_OTSU);
			integral(cplImage, intImage);
			// Compute circle with the maximum density
			float maxDens = 0;
			int idx = 0;
			for (int k = 0; k < ellipses.size(); ++k) {
				int minX = floor(std::max<float>(1, ellipses[k].xc - ellipses[k].a));
				int minY = floor(std::max<float>(1, ellipses[k].yc - ellipses[k].b));
				int maxX = floor(std::min<float>(this->equalizedEye.cols - 1, ellipses[k].xc + ellipses[k].a));
				int maxY = floor(std::min<float>(this->equalizedEye.rows - 1, ellipses[k].yc + ellipses[k].b));
				float D = intImage.at<int>(maxY, maxX) + intImage.at<int>(minY, minX) - intImage.at<int>(minY, maxX) - intImage.at<int>(maxY, minX);
				//D = D / (circles[k][2] * circles[k][2]);
				D = D / ((maxX - minX)*(maxY - minY) * 255);// Max summed luminance is 255*rectangle area!
				if (D > maxDens) {
					maxDens = D;
					idx = k;
				}
			}
			ell = ellipses[idx];

		}

		else {
			ell = ellipses[0];
		}
	}

	return ell;
}


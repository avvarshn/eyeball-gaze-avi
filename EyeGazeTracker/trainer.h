#ifndef TRAINER_H
#define TRAINER_H

#include <vector>
#include <string>

//#include <opencv2/core/core.hpp>
#include <opencv2/ml/ml.hpp>

class Trainer {
public:
    enum POSITION {N, S, E, W, NE, NW, SE, SW, CE};
    enum MODEL {HORIZONTAL, VERTICAL, BOTH};

    /**
     * @brief Add a feature vector to the train set
     * @param feature vector
     * @param vector class
     */
    void addTrainvector (std::vector<float> features, POSITION position);

    /**
     * @brief Train the vertical and horizontal SVM models from the training set
     */
   void trainSVMModels();

    /**
     * @brief Save current SVM models
     * @param models filename base
     */
   void saveSVMModels(std::string fileName);

    /**
     * @brief Load previously stored SVM models
     * @param models filename base
     */
    void loadSVMModels(std::string fileName);

    /**
     * @brief Predict eye center position from the given feature vector
     * @param feature vector
     * @param model type
     * @return estimated eye center position [pxl]
     */
    POSITION predictPositionSVM(std::vector<float> features, MODEL type);

    /**
     * @brief Train the vertical and horizontal regression models from the training set
     */
    void trainRegModels();

    /**
     * @brief Save current regression models
     * @param models filename base
     */
    void saveRegModels(std::string fileName);

    /**
     * @brief Load previously stored regression models
     * @param models filename base
     */
    void loadRegModels(std::string fileName);

    /**
     * @brief Predict eye center position from the given feature vector
     * @param feature vector
     * @param model type
     * @return estimated eye center position [pxl]
     */
    cv::Point2i predictPositionReg(std::vector<float> features, MODEL type);

private:
    cv::SVM modelSVMH;
    cv::SVM modelSVMV;
    cv::SVM modelRegH;
    cv::SVM modelRegV;
    std::vector<float> labelsH;
    std::vector<float> labelsV;
    std::vector<std::vector<float>> dataSet;
};

#endif // TRAINER_H
